package com.pandiandcode.xariti.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pandiandcode.xariti.R;
import com.pandiandcode.xariti.controller.NetworkController;
import com.pandiandcode.xariti.controller.ViewController;
import com.pandiandcode.xariti.model.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ArticleListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Article> mArticles;

    public ArticleListAdapter( Context context, ArrayList<Article> articles ) {

        this.context = context;
        this.mArticles = articles;

    }

    @Override
    public int getCount() {
        return mArticles.size();
    }

    @Override
    public Object getItem( int position ) {

        return mArticles.get( position );

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    /** * Holder for the list items. */
    private class ViewHolder {

        private TextView title;
        private TextView description;
        private ImageView image;

    }

    public View getView( int position, View convertView, ViewGroup parent ) {

        ViewHolder holder;
        Article article = ( Article )getItem( position );
        View viewToUse;

        LayoutInflater mInflater = ( LayoutInflater ) context .getSystemService( Activity.LAYOUT_INFLATER_SERVICE );

        if ( convertView == null ) {

            viewToUse = mInflater.inflate( R.layout.article_item, null );

            holder = new ViewHolder();

            holder.title = ( TextView )viewToUse.findViewById( R.id.article_title );
            holder.description = ( TextView )viewToUse.findViewById( R.id.article_desc );
            holder.image = ( ImageView )viewToUse.findViewById( R.id.article_image );

            viewToUse.setTag( holder );

        } else {

            viewToUse = convertView;
            holder = ( ViewHolder ) viewToUse.getTag();
        }

        holder.title.setText( article.getTitle() );
        holder.description.setText( article.getDescription() );

        if ( !article.getImage().equals( "" ) ) {

            int imageSize = (int) ViewController.dpiToPixel( context, ( ViewController.isSmallScreen( context ) ) ? 150 : 200 );

            Picasso.with( context )
                    .load( NetworkController.IMG_URL + article.getImage() + "&h=" + imageSize + "&w=" + imageSize + "&crop-to-fit&q=100" )
                    .resize( imageSize, imageSize )
                    .centerCrop()
                    .placeholder( R.drawable.holder )
                    .error( R.drawable.holder ).into( holder.image );

        }

        return viewToUse;

    }

}