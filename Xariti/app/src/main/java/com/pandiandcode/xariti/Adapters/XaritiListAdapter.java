package com.pandiandcode.xariti.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pandiandcode.xariti.R;
import com.pandiandcode.xariti.controller.DataController;
import com.pandiandcode.xariti.controller.NetworkController;
import com.pandiandcode.xariti.controller.ViewController;
import com.pandiandcode.xariti.model.ChaBrand;
import com.pandiandcode.xariti.model.CharityDistance;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class XaritiListAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<CharityDistance> mList;

    public XaritiListAdapter( Context context, ArrayList<CharityDistance> items ) {

        super( context, android.R.layout.simple_list_item_1, items );
        this.context = context;
        this.mList = items;

    }


    public Object getItem( int position ) { return mList.get( position ); }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public long getItemId( int position ) {
        return position;
    }

    /** * Holder for the list items. */
    private class ViewHolder{

        TextView xtitle;
        TextView xaddress;
        TextView xdistance;
        ImageView xlogo;

    }


    public View getView ( int position, View convertView, ViewGroup parent ) {

        ViewHolder holder;
        CharityDistance charity = ( CharityDistance )getItem( position );
        View viewToUse;

        LayoutInflater mInflater = ( LayoutInflater ) context .getSystemService( Activity.LAYOUT_INFLATER_SERVICE );

        if ( convertView == null ) {

            viewToUse = mInflater.inflate( R.layout.charity_list_item, null );

            holder = new ViewHolder();

            holder.xtitle = ( TextView )viewToUse.findViewById( R.id.xtitle );
            holder.xaddress = ( TextView )viewToUse.findViewById( R.id.xaddress );
            holder.xdistance = ( TextView )viewToUse.findViewById( R.id.xdistance );
            holder.xlogo = ( ImageView )viewToUse.findViewById( R.id.xlogo );

            viewToUse.setTag( holder );

        } else {

            viewToUse = convertView;
            holder = ( ViewHolder ) viewToUse.getTag();

        }

        if ( charity != null ) {

            //leave a padding at the bottom if it's the last item in the list
            if ( position == getCount() - 1 )
                viewToUse.setPadding( 0, 0, 0, (int) ViewController.dpiToPixel( context,80 ) );

            else
                viewToUse.setPadding( 0, 0, 0, 0 );

            holder.xtitle.setText( charity.getCharity().getTitle() );
            holder.xaddress.setText( charity.getCharity().getAddress() );
            holder.xdistance.setText( String.format( "%.2f miles", charity.getDistance() ) );

            ChaBrand mChaBrand = DataController.getChaBrand( charity.getCharity().getChaBrandID(), context );
            String urlLogo = "";

            if ( mChaBrand != null )
                urlLogo = mChaBrand.getImage();

            int imageSize = (int) ViewController.dpiToPixel( context, 82 );
            Picasso.with( context )
                .load( NetworkController.IMG_URL + urlLogo + "&h=" + imageSize + "&w=" + imageSize + "&crop-to-fit&q=100" )
                .resize( imageSize, imageSize )
                .centerCrop()
                .placeholder( R.drawable.holder )
                .error( R.drawable.holder ).into( holder.xlogo );



        }

        return viewToUse;

    }


    public void update( ArrayList<CharityDistance> charities ) {

        this.mList = charities;
        notifyDataSetChanged();

    }

}
