package com.pandiandcode.xariti.Animations;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/**
 * Created by Perrankana on 09/03/15.
 */
public class AnimWeight extends Animation {

    private float fromWeight;
    private float toWeight;
    private View view;

    public AnimWeight ( float fromWeight, float toWeight, View view ) {

        this.fromWeight = fromWeight;
        this.toWeight = toWeight;
        this.view = view;

    }

    @Override
    protected void applyTransformation( float interpolatedTime, Transformation t ) {

        float newWeight;
        newWeight = fromWeight + ( ( toWeight - fromWeight ) * interpolatedTime );
        LinearLayout.LayoutParams params = ( LinearLayout.LayoutParams ) view.getLayoutParams();
        params.weight = newWeight;
        view.setLayoutParams( params );
        view.requestLayout();

    }

    @Override
    public void initialize( int width, int height, int parentWidth, int parentHeight ) {

        super.initialize( width, height, parentWidth, parentHeight );

    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }

}
