package com.pandiandcode.xariti.Animations;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Perrankana on 06/02/15.
 */
public class AnimWidth extends Animation {

    private int fromWidth;
    private int toWidth;
    private View view;

    public AnimWidth ( int fromWidth, int toWidth, View view ) {

        this.fromWidth = fromWidth;
        this.toWidth = toWidth;
        this.view = view;

    }

    @Override
    protected void applyTransformation( float interpolatedTime, Transformation t ) {

        int newWidth;
        newWidth = fromWidth + ( int )( ( toWidth - fromWidth ) * interpolatedTime );
        view.getLayoutParams().width = newWidth;
        view.requestLayout();

    }

    @Override
    public void initialize( int width, int height, int parentWidth, int parentHeight ) {

        super.initialize( width, height, parentWidth, parentHeight );

    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }

}
