package com.pandiandcode.xariti.AsyncTask;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.pandiandcode.xariti.Listeners.GetLocationListener;
import com.pandiandcode.xariti.XaritiesViewerActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Perrankana on 06/02/15.
 */

public class GetLocationInfo extends AsyncTask< Object, Void, LatLng > {

    private Activity activity;
    private GetLocationListener mLocationListener;

    @Override
    protected LatLng doInBackground( Object... params ) {

        activity = ( Activity )params[1];
        mLocationListener = (GetLocationListener) activity;
        return getLocationInfo( String.valueOf( params[0] ) );

    }

    @Override
    protected void onPostExecute( LatLng latLng ) {

        super.onPostExecute( latLng );

        if ( activity instanceof XaritiesViewerActivity ) {

            if ( latLng.longitude == 0 && latLng.latitude == 0 )
                Toast.makeText( activity,"No Results found",Toast.LENGTH_LONG ).show();

            else
                mLocationListener.onLocationInfoChanged(latLng.latitude, latLng.longitude);

        }

    }


    public static LatLng getLocationInfo( String address ) {

        try {

            address = URLEncoder.encode(address, "UTF-8");

        } catch ( UnsupportedEncodingException e ) { address=""; }

        HttpGet httpGet = new HttpGet( "http://maps.google.com/maps/api/geocode/json?address=" + address + "&ka&sensor=false" );
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {

            response = client.execute( httpGet );
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;

            while ( ( b = stream.read() ) != -1 )
                stringBuilder.append( (char) b );

        } catch ( IOException ignored ) {}

        JSONObject jsonObject;

        double lon = 0;
        double lat = 0;

        try {

            jsonObject = new JSONObject( stringBuilder.toString() );

            lon = ( ( JSONArray ) jsonObject.get("results") ).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lng");

            lat = ( ( JSONArray )jsonObject.get("results") ).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lat");

        } catch ( JSONException e ) {}

        return new LatLng( lat, lon );

    }

}
