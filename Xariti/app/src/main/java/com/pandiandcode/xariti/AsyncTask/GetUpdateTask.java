package com.pandiandcode.xariti.AsyncTask;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.pandiandcode.xariti.Listeners.DataUpdateListener;
import com.pandiandcode.xariti.controller.DataController;
import com.pandiandcode.xariti.controller.NetworkController;
import com.pandiandcode.xariti.model.Article;
import com.pandiandcode.xariti.model.ChaBrand;
import com.pandiandcode.xariti.model.Charity;
import com.pandiandcode.xariti.model.Link;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Perrankana on 06/02/15.
 */
public class GetUpdateTask extends AsyncTask<Void, Integer, Boolean> {

    private Context context;
    private ProgressBar progressBar;
    private DataUpdateListener mUpdateListener;

    public GetUpdateTask( Context context, ProgressBar progressBar ) {

        this.context = context;
        this.progressBar = progressBar;
        this.mUpdateListener = (DataUpdateListener) context;

    }


    protected Boolean doInBackground( Void... urls ) {

        SharedPreferences prefs = context.getSharedPreferences( DataController.PACKAGE, Context.MODE_PRIVATE );

        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost();

        try {

            if ( prefs.getInt("first_time", 0) == 0 )
                request.setURI( new URI( NetworkController.API_URL + "?f=getAllInfo" ) );

            else {

                request.setURI( new URI( NetworkController.API_URL + "?f=getUpdatedCharities" ) );

                List<NameValuePair> nameValuePairs = new ArrayList<>();
                Date dt = new Date();
                DateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
                String lastDate = prefs.getString( "last_update",df.format( dt ) );
                nameValuePairs.add( new BasicNameValuePair( "date", lastDate ) );
                request.setEntity( new UrlEncodedFormEntity( nameValuePairs ) );

            }

            request.setHeader( "Accept", "application/json" );
            HttpResponse response = client.execute( request );

            BufferedReader in = new BufferedReader( new InputStreamReader( response.getEntity().getContent() ) );
            StringBuilder sb = new StringBuilder( "" );
            String line;
            String NL = System.getProperty( "line.separator" );

            while ( ( line = in.readLine() ) != null )
                sb.append( line ).append( NL );

            in.close();
            String page = sb.toString();
            JSONObject jresponse = new JSONObject( page );
            String status = jresponse.getString( "status" );
            JSONObject jData = jresponse.getJSONObject( "data" );

            if ( status.equals( "ok" ) ) {

                if ( prefs.getInt( "first_time", 0 ) == 0 ) {

                    JSONArray jChaBrands = jData.getJSONArray( "chaBrands" );
                    JSONArray jCharities = jData.getJSONArray( "charities" );
                    JSONArray jLinks = jData.getJSONArray( "links" );
                    JSONArray jArticles = jData.getJSONArray( "articles" );

                    ArrayList<ChaBrand> chaBrands = NetworkController.getChaBrandsFromJSON( jChaBrands );
                    publishProgress(25);

                    ArrayList<Charity> charities = NetworkController.getCharitiesFromJSON( jCharities );
                    publishProgress(50);

                    ArrayList<Link> links = NetworkController.getLinksFromJSON( jLinks );
                    publishProgress(75);

                    ArrayList<Article> articles = NetworkController.getArticlesFromJSON( jArticles );
                    publishProgress(90);

                    DataController.insertData( chaBrands, charities, links, articles, context );

                    prefs.edit().putInt( "first_time", 1 ).apply();

                    return true;

                } else {

                    JSONArray jChaBrands = jData.getJSONArray( "chaBrands" );
                    JSONArray jCharities = jData.getJSONArray( "charities" );
                    JSONArray jLinks = jData.getJSONArray( "links" );
                    JSONArray jArticles = jData.getJSONArray( "articles" );
                    JSONArray jChasDelete = jData.getJSONArray( "deleted" );

                    ArrayList<ChaBrand> chaBrands = NetworkController.getChaBrandsFromJSON( jChaBrands );
                    publishProgress(20);

                    ArrayList<Charity> charities = NetworkController.getCharitiesFromJSON( jCharities );
                    publishProgress(40);

                    ArrayList<Link> links = NetworkController.getLinksFromJSON( jLinks );
                    publishProgress(50);

                    ArrayList<Article> articles = NetworkController.getArticlesFromJSON( jArticles );
                    publishProgress(70);

                    ArrayList<Charity> delCharities = NetworkController.getCharitiesFromJSON( jChasDelete );
                    publishProgress(90);

                    DataController.updateData( chaBrands, charities, links, articles, context );
                    DataController.removeCharities( delCharities, context );

                    return true;

                }

            }

            return false;

        } catch ( URISyntaxException | JSONException | IOException | ParseException e ) {

            return false;

        }

    }


    protected void onPostExecute( Boolean feed ) {

        progressBar.setProgress(100);
        mUpdateListener.onUpdateCompleted();

    }

    @Override
    protected void onProgressUpdate( Integer... values ) {

        super.onProgressUpdate( values );
        progressBar.setProgress( values[0] );

    }

}