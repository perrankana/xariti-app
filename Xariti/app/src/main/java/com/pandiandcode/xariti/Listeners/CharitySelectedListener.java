package com.pandiandcode.xariti.Listeners;

/**
 * Created by Perrankana on 13/03/15.
 */
public interface CharitySelectedListener {
    public void onCharityPicked( int charityID );
    public void onSearchLocation();
    public boolean isSearch();
}
