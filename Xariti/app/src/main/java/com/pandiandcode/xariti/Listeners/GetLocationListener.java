package com.pandiandcode.xariti.Listeners;

/**
 * Created by Perrankana on 13/03/15.
 */
public interface GetLocationListener {

    public void onLocationInfoChanged( double latitude, double longitude);

}
