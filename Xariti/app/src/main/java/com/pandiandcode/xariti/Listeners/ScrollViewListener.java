package com.pandiandcode.xariti.Listeners;

import com.pandiandcode.xariti.view.MyScrollView;


public interface ScrollViewListener {

    void onScrollChanged( MyScrollView scrollView, int x, int y, int oldx, int oldy );

}
