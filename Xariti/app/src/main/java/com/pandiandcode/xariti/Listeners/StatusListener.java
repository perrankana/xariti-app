package com.pandiandcode.xariti.Listeners;

/**
 * Created by Perrankana on 13/03/15.
 */
public interface StatusListener {
    public void setStatus(int status);
    public int getStatus();
    public boolean isInTwoPaneMode();
}
