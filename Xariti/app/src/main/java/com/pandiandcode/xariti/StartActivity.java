package com.pandiandcode.xariti;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.pandiandcode.xariti.AsyncTask.GetUpdateTask;
import com.pandiandcode.xariti.Listeners.DataUpdateListener;
import com.pandiandcode.xariti.controller.NetworkController;


public class StartActivity extends Activity implements DataUpdateListener{

    private ProgressBar mProgress;
    private int status;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {

        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_start );

        mProgress = ( ProgressBar )findViewById( R.id.progressBar );

        status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        if ( status == ConnectionResult.SUCCESS ) {

            if ( NetworkController.isInternetAvailable( this ) )
                new GetUpdateTask( this, mProgress ).execute();

            else {

                mProgress.setProgress( 100 );
                Toast.makeText( this, "No Internet Connection", Toast.LENGTH_LONG ).show();

                goToActivity();

            }

        } else {

            runOnUiThread(new Runnable() {
                public void run() {
                    final Dialog dialog = GooglePlayServicesUtil.getErrorDialog( status, StartActivity.this, status );
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finish();
                        }
                    });
                    if ( dialog == null )
                        Toast.makeText( StartActivity.this,"incompatible version of Google Play Services",Toast.LENGTH_LONG ).show();
                    else
                        dialog.show();

                }
            });

        }

    }

    public void goToActivity(){

        Intent intent = new Intent( this, XaritiesViewerActivity.class );
        startActivity( intent );
        finish();

    }

    @Override
    public void onUpdateCompleted() {
        goToActivity();
    }
}
