package com.pandiandcode.xariti;


import android.animation.Animator;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.pandiandcode.xariti.Animations.AnimWeight;
import com.pandiandcode.xariti.Animations.AnimWidth;
import com.pandiandcode.xariti.AsyncTask.GetLocationInfo;
import com.pandiandcode.xariti.Listeners.CharitySelectedListener;
import com.pandiandcode.xariti.Listeners.GetLocationListener;
import com.pandiandcode.xariti.Listeners.StatusListener;
import com.pandiandcode.xariti.controller.DataController;
import com.pandiandcode.xariti.controller.GeoController;
import com.pandiandcode.xariti.controller.ViewController;
import com.pandiandcode.xariti.fragments.ArticleListFragment;
import com.pandiandcode.xariti.fragments.CharityListFragment;
import com.pandiandcode.xariti.fragments.CharityMapFragment;
import com.pandiandcode.xariti.fragments.InfoFragment;
import com.pandiandcode.xariti.fragments.XarityFragment;

public class XaritiesViewerActivity
        extends FragmentActivity
        implements CharitySelectedListener, GetLocationListener, StatusListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final int MAP_STATUS = 1;
    public static final int LIST_STATUS = 2;
    public static final int XARITY_STATUS = 3;
    public static final int ARTICLE_STATUS = 4;

    private CharityListFragment mListFragment;
    private CharityMapFragment mMapFragment;
    private XarityFragment mXarityFragment;
    private ArticleListFragment mArticlesFragment;
    private InfoFragment mInfoFragment;

    private FragmentManager mFragmentManager;

    private Location mLastLocation;
    private GoogleApiClient mLocationClient;

    private EditText mSearchText;
    private ToggleButton mToggle;
    private TextView mXarityTitle;
    private ImageButton mBSearch;
    private ImageView mABLogo;

    private RelativeLayout mXarityActionBar;
    private RelativeLayout mActionBar;
    private RelativeLayout mArticlesLayout;
    private RelativeLayout mArticleDetailLayout;
    private RelativeLayout mContentLayout;
    private RelativeLayout mInfoLayout;

    private boolean isSearch;
    private int mCharityID;
    private int status;
    private int previousStatus;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {

        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_xarities_viewer );

        mXarityActionBar = ( RelativeLayout ) findViewById( R.id.xarity_actionbar );
        mActionBar = ( RelativeLayout ) findViewById( R.id.action_bar );
        mArticlesLayout = ( RelativeLayout ) findViewById( R.id.articles_layout );
        mArticleDetailLayout = ( RelativeLayout ) findViewById( R.id.article_detail_layout );
        mContentLayout = ( RelativeLayout ) findViewById( ( isInTwoPaneMode() ) ? R.id.fragment_container_large : R.id.fragment_container);
        mInfoLayout = ( RelativeLayout ) findViewById( R.id.finfo_layout );
        mXarityTitle = ( TextView ) findViewById( R.id.xarity_abtitle );
        mSearchText = ( EditText ) findViewById( R.id.search_text );
        mToggle = ( ToggleButton ) findViewById( R.id.btn_toggle );
        mBSearch = ( ImageButton ) findViewById( R.id.btn_search );
        mABLogo = ( ImageView ) findViewById( R.id.ablogo );

        mXarityActionBar.setVisibility( View.GONE );
        mArticlesLayout.setTag( "inactive" );

        mInfoLayout.setTranslationX( -ViewController.dpiToPixel( this,180 ) );
        mInfoLayout.setTag( "inactive" );

        if ( savedInstanceState != null ) {

            onRestoreInstanceState( savedInstanceState );

        } else {

            isSearch = false;
            status = MAP_STATUS;

        }

        if ( ViewController.isLandscape( this ) ) {

            mArticlesLayout.setX( ViewController.dpiToPixel( this,( ViewController.isSmallScreen( this ) ) ? 170 : 220 ) );

        } else {

            mArticlesLayout.setY(ViewController.dpiToPixel( this, ( ViewController.isSmallScreen( this ) ) ? 170 : 220 ) );

        }
        mArticlesLayout.setVisibility( View.VISIBLE );

        mLocationClient = new GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build();


        mToggle.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged( CompoundButton buttonView, boolean isChecked ) {

                flipFragments( mToggle.isChecked() );

            }

        });

        addInfoFragment();

        mABLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open Info app
                if ( mInfoLayout.getTag().equals("inactive") ) {

                    mInfoLayout.setTag( "active" );
                    mInfoLayout.animate().translationX(0);

                } else {

                    mInfoLayout.setTag("inactive");
                    mInfoLayout.animate().translationX( -ViewController.dpiToPixel( getApplication(), 180 ) );

                }

            }
        });

        initFragments();


        switch( status ) {
            case XARITY_STATUS:
                openXarity( mCharityID, isInTwoPaneMode() ? R.id.fragment_container_large : R.id.fragment_container );
                if(previousStatus == LIST_STATUS) mToggle.setChecked(true);
                break;

            case ARTICLE_STATUS:
                openXarity( mCharityID, isInTwoPaneMode() ? R.id.fragment_container_large : R.id.fragment_container );
                if(previousStatus == LIST_STATUS) mToggle.setChecked(true);
                break;

            case LIST_STATUS:
                mToggle.setChecked(true);
                break;

            default:
                status = MAP_STATUS;

        }

        mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ( ( event != null && ( event.getKeyCode() == KeyEvent.KEYCODE_ENTER ) ) || ( actionId == EditorInfo.IME_ACTION_DONE ) )
                    doSearch();

                return false;
            }
        });

        mBSearch.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick( View v ) {

                ViewController.hideKeyboard( XaritiesViewerActivity.this );
                doSearch();

            }

        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        mLocationClient.connect();

    }

    @Override
    protected void onSaveInstanceState( Bundle outState ) {

        outState.putBoolean( "isSearch", isSearch );
        outState.putInt("status", status);
        outState.putInt( "charityID", mCharityID );
        outState.putInt("previousStatus",previousStatus);

    }

    @Override
    protected void onRestoreInstanceState( Bundle savedInstanceState ) {

        isSearch = savedInstanceState.getBoolean( "isSearch" );
        status = savedInstanceState.getInt( "status" );
        mCharityID = savedInstanceState.getInt( "charityID" );
        previousStatus = savedInstanceState.getInt("previousStatus");

    }

    @Override
    protected void onPause() {

        super.onPause();
        mLocationClient.disconnect();

    }

    @Override
    public void onBackPressed() {

        if(mInfoLayout.getTag().equals("active")){

            mInfoLayout.animate().translationX( -ViewController.dpiToPixel( getApplication(), 180 ) );
            mInfoLayout.setTag("inactive");

        }else{

            switch( status ) {
                case XARITY_STATUS:
                    mXarityActionBar.setVisibility( View.GONE );
                    mActionBar.setVisibility( View.VISIBLE );
                    removeXarity();
                    break;

                case ARTICLE_STATUS:
                    mXarityActionBar.setVisibility( View.GONE );
                    mActionBar.setVisibility( View.VISIBLE );
                    removeXarity();
                    break;

                case LIST_STATUS:
                    flipFragments(false);
                    mToggle.setChecked(false);
                    break;

                default:
                    super.onBackPressed();

            }
        }

    }

    /**
     * CharitySelectedListener interface method that runs when a Charity is selected in Map or List views
     * @param charityID charity shop ID
     */
    @Override
    public void onCharityPicked( int charityID ) {

        mCharityID = charityID;
        openXarity( charityID, isInTwoPaneMode() ? R.id.fragment_container_large : R.id.fragment_container );

    }

    /**
     * GetLocationListener interface method that runs when the current location is requested in MapFragment
     */
    @Override
    public void onSearchLocation() {

        isSearch = false;
        onConnected(null);
        if ( mLastLocation != null )
            updateFragments( mLastLocation.getLatitude(), mLastLocation.getLongitude() );

        if ( mSearchText.getVisibility() == View.VISIBLE ) {

            mSearchText.setText("");
            mSearchText.setVisibility(View.INVISIBLE);

        }

    }

    /**
     * If EditText mSearchText is visible and contains a valid value, the system will get the
     * location of the search value. If EditText mSearchText wasn't visible, it'll become visible
     * and the user will be able to fill this EditText for a Search.
     */
    public void doSearch(){

        if ( mSearchText.getVisibility() == View.VISIBLE ) {

            if ( mSearchText.getText().toString().equals("") ) {

                mSearchText.setVisibility(View.GONE);
                isSearch = false;

            } else {

                LatLng mll = GeoController.getLocationFromAddress(mSearchText.getText().toString(), getApplicationContext());

                if ( mll != null ) {

                    saveLocationInPrefs( mll.latitude, mll.longitude );
                    updateFragments( mll.latitude, mll.longitude );

                } else
                    new GetLocationInfo().execute( mSearchText.getText().toString(), XaritiesViewerActivity.this );

            }

        } else {

            mSearchText.setVisibility( View.VISIBLE );
            mSearchText.setFocusable( true );
            mSearchText.setFocusableInTouchMode( true );
            mSearchText.requestFocus();
            ViewController.showKeyboard( getApplicationContext(), mSearchText );
            isSearch = true;

        }
    }

    /**
     * Hide ActionBar and Shows Charity Action Bar, resize layout if needed and adds XarityFragment
     * @param charityID Charity Shop ID
     * @param fragmentContainerID Container Layout ID
     */
    public void openXarity( int charityID, int fragmentContainerID ){

        status = ( status == ARTICLE_STATUS ) ? XARITY_STATUS : status;
        if ( mInfoLayout.getTag().equals("active") ) {

            mInfoLayout.animate().translationX( -ViewController.dpiToPixel( this, 180 ) );
            mInfoLayout.setTag("inactive");

        }

        mXarityActionBar.setVisibility( View.VISIBLE );
        mXarityActionBar.setAlpha( 0 );
        mXarityActionBar.setTag( "inactive" );
        mXarityTitle.setText( DataController.getCharity(charityID, this).getTitle() );

        if ( !isInTwoPaneMode() )
            mActionBar.animate().translationY(-ViewController.dpiToPixel(this, 60));

        if ( !DataController.getCharity(charityID, this).getArticles().isEmpty() ) {

            if ( ViewController.isLandscape( this ) ) {

                int fullWidth = ViewController.getWidthScreen( this );
                int articlesWidth = ( int ) ViewController.dpiToPixel( this, ( ViewController.isSmallScreen( this ) ) ? 150 : 200 );

                AnimWidth  a = new AnimWidth( fullWidth, fullWidth - articlesWidth, mContentLayout );
                a.setDuration( 500 );
                mContentLayout.startAnimation( a );

                if ( isInTwoPaneMode() ) {

                    RelativeLayout mFXaritiLayout = ( RelativeLayout )findViewById( R.id.fxariti_layout );
                    LinearLayout.LayoutParams params = ( LinearLayout.LayoutParams ) mFXaritiLayout.getLayoutParams();
                    params.weight = 0.4f;
                    mFXaritiLayout.setLayoutParams( params );
                    mFXaritiLayout.requestLayout();

                }

                mArticlesLayout.animate().translationX(0);

            } else
                mArticlesLayout.animate().translationY(0);

            mArticlesLayout.setTag("active");

        } else if ( isInTwoPaneMode() && ViewController.isLandscape( this ) ) {

            mMapFragment.getSnapShot();

            RelativeLayout mFXaritiLayout = ( RelativeLayout )findViewById( R.id.fxariti_layout );
            LinearLayout.LayoutParams params = ( LinearLayout.LayoutParams ) mFXaritiLayout.getLayoutParams();
            params.weight = 0.6f;
            mFXaritiLayout.setLayoutParams( params );
            mFXaritiLayout.requestLayout();

        }

        if ( isInTwoPaneMode() ) {

            RelativeLayout mFMapLayout = ( RelativeLayout ) findViewById( R.id.fmap_layout );
            LinearLayout.LayoutParams params = ( LinearLayout.LayoutParams ) mFMapLayout.getLayoutParams();

            if ( params.weight == 1 ) {

                AnimWeight a = new AnimWeight( 1, 0.4f, mFMapLayout );
                a.setDuration( 500 );
                mFMapLayout.startAnimation(a);

            }

        }

        openXarityFragment( charityID, fragmentContainerID );

        if ( status != XARITY_STATUS )
            previousStatus = status;

        status = XARITY_STATUS;
        mCharityID = charityID;

    }

    public void removeXarity(){

        mXarityActionBar.setVisibility( View.INVISIBLE );
        mXarityActionBar.setAlpha(0);

        if ( isInTwoPaneMode() ) {

            RelativeLayout mFMapLayout = ( RelativeLayout )findViewById( R.id.fmap_layout );
            AnimWeight a = new AnimWeight( 0.4f, 1, mFMapLayout );
            a.setDuration( 300 );
            a.setAnimationListener( new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                    ImageView mSnatShot = ( ImageView )findViewById( R.id.snapshot );
                    mSnatShot.setVisibility( View.VISIBLE );

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    ImageView mSnatShot = ( ImageView )findViewById( R.id.snapshot );
                    try { Thread.sleep(100); } catch ( InterruptedException e ) {}
                    mSnatShot.setVisibility( View.GONE );

                }

                @Override
                public void onAnimationRepeat(Animation animation) {}
            });

            mFMapLayout.startAnimation( a );

        } else
            mActionBar.animate().translationY( -ViewController.dpiToPixel( this, 0 ) );

        if ( mArticlesLayout.getTag().equals("active") ) {

            if ( ViewController.isLandscape( this ) ) {

                mArticlesLayout.animate().translationX(ViewController.dpiToPixel(this, (ViewController.isSmallScreen(this)) ? 170 : 220));
                int fullWidth = ViewController.getWidthScreen( this );
                int articlesWidth = (int) ViewController.dpiToPixel( this, ( ViewController.isSmallScreen( this ) ) ? 150 : 200 );

                AnimWidth a = new AnimWidth( fullWidth - articlesWidth, fullWidth, mContentLayout );
                a.setDuration( 300 );
                mContentLayout.startAnimation( a );

            }else
                mArticlesLayout.animate().translationY(ViewController.dpiToPixel(this, (ViewController.isSmallScreen(this)) ? 170 : 220));

            mArticlesLayout.setTag("inactive");

        }

        removeXarityFragment();

        if ( !isInTwoPaneMode() ) {

            status = previousStatus;
            previousStatus = 0;

        } else
            status = MAP_STATUS;

    }

    /**
     *
     * @return status MAP_STATUS | LIST_STATUS | XARITY_STATUS | ARTICLE_STATUS
     */
    public int getStatus(){
        return status;
    }

    /**
     * Changes the status
     * @param status MAP_STATUS | LIST_STATUS | XARITY_STATUS | ARTICLE_STATUS
     */
    public void setStatus(int status){
        this.status = status;
    }

    /**
     * @return true if it's in search mode, false otherwise
     */
    public boolean isSearch(){ return isSearch; }

    /**
     * @return true if the screen is big enough to fit two Panels, false otherwise
     */
    public boolean isInTwoPaneMode() {

        return findViewById( R.id.fragment_container_large ) != null;

    }

    /***************/
    /** FRAGMENTS **/
    /***************/

    /**
     * Initializes Map Fragment and List Fragments, and shows the fragment depending of status.
     */
    private void initFragments() {

        mMapFragment = new CharityMapFragment();

        if ( mCharityID != 0 ) {

            Bundle args = new Bundle();
            args.putInt( "charityID", mCharityID );
            mMapFragment.setArguments( args );

        }

        mListFragment = new CharityListFragment();

        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.add( R.id.fragment_container, mMapFragment );

        if ( !isInTwoPaneMode() ) {

            ft.add( R.id.fragment_container, mListFragment );

            if ( mToggle.isChecked() || status == LIST_STATUS || previousStatus == LIST_STATUS ){
                ft.hide( mMapFragment );
            }
            else
                ft.hide( mListFragment );

        }

        ft.commit();

    }

    /**
     * Switch between List or Map Fragment depending on isList
     * @param isList boolean, indicates if it should change to List or Map Fragment
     */
    private void flipFragments( boolean isList ) {

        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setTransition( FragmentTransaction.TRANSIT_FRAGMENT_FADE );

        if ( isList ) {

            ft.hide( mMapFragment );
            ft.show( mListFragment );
            status = LIST_STATUS;

        } else {

            ft.hide( mListFragment );
            ft.show( mMapFragment );
            status = MAP_STATUS;

        }

        ft.commit();

    }

    private void addInfoFragment() {

        if ( mInfoFragment == null )
            mInfoFragment = new InfoFragment();

        mFragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        fragmentTransaction.replace( R.id.finfo_layout, mInfoFragment );

        fragmentTransaction.commit();

    }

    /**
     * Adds Charity Fragment
     * @param charityID Charity Shop ID
     * @param fragmentContainerID Container Layout ID
     */
    public void openXarityFragment( int charityID, int fragmentContainerID ){

        Bundle args = new Bundle();
        args.putInt( "charityID", charityID );

        mXarityFragment = new XarityFragment();
        mXarityFragment.setArguments( args );

        FragmentTransaction transaction = mFragmentManager.beginTransaction();

        if ( isInTwoPaneMode() ) {

            if (!ViewController.isLandscape(this))
                transaction.setCustomAnimations(R.animator.scale_in_animation, R.animator.slide_out_bottom_animation);

            else
                transaction.setCustomAnimations(R.animator.scale_in_animation, R.animator.slide_out_right_animation);

            if (status == XARITY_STATUS){
                transaction.replace(fragmentContainerID, mXarityFragment);
                if ( mArticlesFragment != null )
                    transaction.remove(mArticlesFragment);
            }else
                transaction.add( fragmentContainerID, mXarityFragment );

        } else {

            transaction.setCustomAnimations( R.animator.slide_in_right_animation, R.animator.scale_out_animation );
            transaction.add( fragmentContainerID, mXarityFragment );

        }

        mArticlesFragment = null;

        if ( !DataController.getCharity(charityID, this).getArticles().isEmpty() ) {

            mArticlesFragment = new ArticleListFragment();
            mArticlesFragment.setArguments( args );

            transaction.add( R.id.articles_layout, mArticlesFragment );

        }

        transaction.commit();

    }

    public void removeXarityFragment( ) {

        FragmentTransaction transaction = mFragmentManager.beginTransaction();

        if ( isInTwoPaneMode() && !ViewController.isLandscape( this ) )
            transaction.setCustomAnimations( R.animator.scale_in_animation, R.animator.slide_out_bottom_animation );

        else
            transaction.setCustomAnimations( R.animator.scale_in_animation, R.animator.slide_out_right_animation );

        if ( mArticlesFragment != null ) {

            transaction.remove( mArticlesFragment );

            if ( status == ARTICLE_STATUS ) {

                if ( ViewController.isLandscape( this ) ) {
                    mArticleDetailLayout.animate().translationX( ViewController.getWidthScreen(this) - ViewController.dpiToPixel( this, ( ViewController.isSmallScreen( this ) ) ? 170 : 220 ) ).setListener( new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {}

                            @Override
                            public void onAnimationEnd( Animator animation ) {

                                if ( mArticlesLayout.getTag().equals("inactive") )
                                    mArticleDetailLayout.setVisibility(View.GONE);

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {}

                            @Override
                            public void onAnimationRepeat(Animator animation) {}

                    });

                } else {

                    mArticleDetailLayout.animate().translationY( ViewController.getHeightScreen(this) - ViewController.dpiToPixel( this, ( ViewController.isSmallScreen( this ) ) ? 170 : 220 ) ).setListener( new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {}

                            @Override
                            public void onAnimationEnd(Animator animation) {

                                if ( mArticlesLayout.getTag().equals("inactive") )
                                    mArticleDetailLayout.setVisibility( View.GONE );

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {}

                            @Override
                            public void onAnimationRepeat(Animator animation) {}

                    });

                }

            } else
                mArticleDetailLayout.setVisibility(View.GONE);

        }

        transaction.remove( mXarityFragment );
        transaction.commit();

    }

    /**
     * Update Map Fragment and List Fragment with the new coordinates
     * @param latitude
     * @param longitude
     */
    public void updateFragments( double latitude, double longitude ) {

        if ( mListFragment != null && !isInTwoPaneMode() )
            mListFragment.updateList( latitude, longitude );

        if ( mMapFragment != null )
            mMapFragment.updateMap();

    }

    /**************/
    /** LOCATION **/
    /**************/

    public void saveLocationInPrefs( double latitude, double longitude) {

        SharedPreferences prefs = getSharedPreferences( DataController.PACKAGE, Context.MODE_PRIVATE );
        prefs.edit().putString( "currentLat", latitude+"" ).apply();
        prefs.edit().putString( "currentLong", longitude+"" ).apply();

    }

    @Override
    public void onLocationInfoChanged(double latitude, double longitude) {
        saveLocationInPrefs(latitude, longitude);
        updateFragments( latitude, longitude );
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLastLocation =LocationServices.FusedLocationApi.getLastLocation(mLocationClient);

        if( mLastLocation != null )
            saveLocationInPrefs(mLastLocation.getLatitude(),mLastLocation.getLongitude());
        else
            Toast.makeText(this,getResources().getText(R.string.location_no_loaded),Toast.LENGTH_LONG).show();
    }


    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this,getResources().getText(R.string.location_suspended), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this,getResources().getText(R.string.location_failed), Toast.LENGTH_SHORT).show();
    }
}
