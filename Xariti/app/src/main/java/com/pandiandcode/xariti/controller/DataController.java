package com.pandiandcode.xariti.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

import com.pandiandcode.xariti.model.Article;
import com.pandiandcode.xariti.model.ChaBrand;
import com.pandiandcode.xariti.model.Charity;
import com.pandiandcode.xariti.model.CharityDistance;
import com.pandiandcode.xariti.model.Link;
import com.pandiandcode.xariti.modelDAO.ArticleDAO;
import com.pandiandcode.xariti.modelDAO.ChaBrandDAO;
import com.pandiandcode.xariti.modelDAO.CharityDAO;
import com.pandiandcode.xariti.modelDAO.LinkDAO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class DataController {

    public static String PACKAGE = "com.pandiandcode.xariti";

    public static void insertData( ArrayList<ChaBrand> chaBrands, ArrayList<Charity> charities, ArrayList<Link> links, ArrayList<Article> articles, Context context ) {

        insertChaBrands( chaBrands, context );
        insertCharities( charities, context );
        insertLinks( links, context );
        insertArticles( articles, context );

        Date dt = new Date();
        DateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );

        SharedPreferences prefs = context.getSharedPreferences( PACKAGE, Context.MODE_PRIVATE );
        prefs.edit().putString( "last_update", df.format( dt ) ).apply();

    }


    public static void updateData( ArrayList<ChaBrand> chaBrands, ArrayList<Charity> charities, ArrayList<Link> links, ArrayList<Article> articles, Context context ) {

        for( Charity charity : charities ) {

            removeArticles( charity.getCharityID(),context );
            removeLinks( charity.getCharityID(),context );
            removeCharity( charity.getCharityID(),context );

        }

        for ( ChaBrand chaBrand: chaBrands )
            updateChaBrand(chaBrand,context);

        insertCharities( charities, context );
        insertLinks( links, context );
        insertArticles( articles, context );

        Date dt = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        SharedPreferences prefs = context.getSharedPreferences( PACKAGE, Context.MODE_PRIVATE );
        prefs.edit().putString( "last_update", df.format( dt ) ).apply();

    }


    public static void removeCharities( ArrayList<Charity> charities, Context context ) {

        for( Charity charity : charities ) {

            removeArticles( charity.getCharityID(),context );
            removeLinks( charity.getCharityID(),context );
            removeCharity( charity.getCharityID(),context );

        }

    }


    public static void insertChaBrands( ArrayList<ChaBrand> chaBrands, Context context ) {

        ChaBrandDAO chaBrandDAO= new ChaBrandDAO( context );
        chaBrandDAO.bulk( chaBrands );

    }


    public static void insertCharities( ArrayList<Charity> charities, Context context ) {

        CharityDAO charityDAO = new CharityDAO( context );
        charityDAO.bulk( charities );

    }


    public static void insertLinks( ArrayList<Link> links, Context context ) {

        LinkDAO linkDAO = new LinkDAO( context );
        linkDAO.bulk( links );

    }


    public static void insertArticles( ArrayList<Article> articles, Context context ) {

        ArticleDAO articleDAO= new ArticleDAO( context );
        articleDAO.bulk( articles );

    }


    public static ArrayList<CharityDistance> getCharities( double lat, double longi, double distance, boolean isKm, Context context ) {

        double distKm;
        if ( !isKm )
            distKm = distance * 1.6093f;
        else
            distKm = distance;

        double nlat = ( lat * 113.3f + distKm ) / 113.3f;
        double slat = ( lat * 113.3f - distKm ) / 113.3f;

        double elong = ( longi * 111.11f + distKm*1.5f ) / 111.11f;
        double wlong = ( longi * 111.11f - distKm*1.5f ) / 111.11f;

        ArrayList<Charity> charities = getCharities( nlat, slat, wlong, elong, context );
        ArrayList<CharityDistance> cDistances = new ArrayList<>();

        for ( Charity charity : charities )
            cDistances.add( new CharityDistance( charity,getDistances( lat, longi, charity.getLatitude(), charity.getLongitude() ) / 1000 ) );

        Collections.sort( cDistances );
        return cDistances;

    }


    public static ArrayList<Charity> getCharities( double nlat, double slat, double wlong, double elong, Context context) {

        CharityDAO charityDAO = new CharityDAO( context );
        return charityDAO.selectCharities( nlat, slat, wlong, elong );

    }


    public static Charity getCharity( int charityID, Context context ) {

        LinkDAO linkDAO = new LinkDAO( context );
        ArrayList<Link> links = linkDAO.selectLinks( charityID );

        ArticleDAO articleDAO = new ArticleDAO( context );
        ArrayList<Article> articles = articleDAO.selectArticles( charityID );

        CharityDAO charityDAO = new CharityDAO( context );
        Charity charity = charityDAO.select( charityID );

        if ( charity != null ) {

            charity.setArticles( articles );
            charity.setLinks( links );

        }

        return charity;

    }


    public static ChaBrand getChaBrand( int chaBrandID, Context context ) {

        ChaBrandDAO chaBrandDAO = new ChaBrandDAO( context );
        return chaBrandDAO.select( chaBrandID );

    }


    public static boolean updateChaBrand( ChaBrand chaBrand, Context context ) {

        ChaBrandDAO chaBrandDAO = new ChaBrandDAO( context );
        if(chaBrandDAO.select(chaBrand.getChaBrandID()) == null)
            return chaBrandDAO.insert(chaBrand);

        else
            return chaBrandDAO.updateChaBrand( chaBrand );

    }


    public static boolean removeCharity( int charityID, Context context ) {

        CharityDAO charityDAO = new CharityDAO( context );
        return charityDAO.delete( charityID );

    }


    public static void removeLinks( int charityID, Context context ) {

        LinkDAO linkDAO = new LinkDAO( context );
        linkDAO.deleteAll( charityID );

    }


    public static void removeArticles( int charityID, Context context ) {

        ArticleDAO articleDAO = new ArticleDAO( context );
        articleDAO.deleteAll( charityID );

    }


    public static double getDistances( double fromLat, double fromLong, double toLat, double toLong ) {

        Location selected_location = new Location("fromLocation");
        Location near_locations=new Location("toLocation");

        selected_location.setLatitude( fromLat );
        selected_location.setLongitude( fromLong );
        near_locations.setLatitude( toLat );
        near_locations.setLongitude( toLong );

        return selected_location.distanceTo( near_locations );

    }

}
