package com.pandiandcode.xariti.controller;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

/**
 * Created by Perrankana on 06/02/15.
 */
public class GeoController {

    public static LatLng getLocationFromAddress( String strAddress, Context context ) {

        Geocoder coder = new Geocoder( context );
        List<Address> address;
        LatLng p1;

        try {

            address = coder.getFromLocationName( strAddress, 5 );

            if ( address == null || address.size() == 0 ) {
                return null;
            }

            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng( location.getLatitude(),location.getLongitude() );

            return p1;

        } catch ( IOException ioe ) {
            return null;
        }

    }

}
