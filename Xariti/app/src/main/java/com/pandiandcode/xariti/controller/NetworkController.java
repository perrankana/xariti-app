package com.pandiandcode.xariti.controller;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.pandiandcode.xariti.model.Article;
import com.pandiandcode.xariti.model.ChaBrand;
import com.pandiandcode.xariti.model.Charity;
import com.pandiandcode.xariti.model.Link;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class NetworkController {


    public static String URL = "http://xariti.pandiandcode.com/";
    public static String API_URL = "http://xariti.pandiandcode.com/api/AnonimoIndex.php";
    public static String IMG_URL = "http://xariti.pandiandcode.com/img.php?src=";


    public static boolean isInternetAvailable( Context context ) {

        ConnectivityManager cm = ( ConnectivityManager ) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;

    }

    public static ArrayList<ChaBrand> getChaBrandsFromJSON( JSONArray jChaBrands ) throws JSONException {

        ArrayList<ChaBrand> chaBrands = new ArrayList<>();

        for ( int i = 0; i < jChaBrands.length(); i++ ) {

            JSONObject json = jChaBrands.getJSONObject( i );
            chaBrands.add( new ChaBrand( json.getInt("chabrandID"), json.getString("title"), json.getString("description"), json.getString("image"), json.getString("website") ) );

        }

        return chaBrands;

    }


    public static ArrayList<Charity> getCharitiesFromJSON( JSONArray jCharities ) throws JSONException, ParseException {

        ArrayList<Charity> charities = new ArrayList<>();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        for ( int i = 0; i < jCharities.length(); i++ ) {

            JSONObject json = jCharities.getJSONObject( i );
            charities.add( new Charity( json.getInt("charityID"), json.getString("title"), json.getString("description"), json.getString("address"), json.getString("postcode"), json.getDouble("latitude"), json.getDouble("longitude"), format.parse(json.getString("date")), json.getInt("chabrandID") ) );

        }

        return charities;

    }


    public static ArrayList<Link> getLinksFromJSON( JSONArray jLinks ) throws JSONException {

        ArrayList<Link> links = new ArrayList<>();

        for ( int i = 0; i < jLinks.length(); i++ ) {

            JSONObject json = jLinks.getJSONObject( i );
            links.add( new Link( json.getInt("linkID"), json.getString("title"), json.getString("link"), json.getInt("charityID") ) );

        }

        return links;

    }


    public static ArrayList<Article> getArticlesFromJSON( JSONArray jArticles ) throws JSONException, ParseException {

        ArrayList<Article> articles = new ArrayList<>();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        for ( int i = 0; i < jArticles.length(); i++ ) {

            JSONObject json = jArticles.getJSONObject( i );
            articles.add( new Article( json.getInt("articleID"), json.getString("title"), json.getString("description"), json.getString("image"), format.parse(json.getString("date")), json.getInt("charityID"), json.getInt("image_w"), json.getInt("image_h") ) );

        }

        return articles;

    }


}
