package com.pandiandcode.xariti.fragments;


import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.pandiandcode.xariti.Adapters.ArticleListAdapter;
import com.pandiandcode.xariti.Listeners.StatusListener;
import com.pandiandcode.xariti.R;
import com.pandiandcode.xariti.XaritiesViewerActivity;
import com.pandiandcode.xariti.controller.DataController;
import com.pandiandcode.xariti.controller.NetworkController;
import com.pandiandcode.xariti.controller.ViewController;
import com.pandiandcode.xariti.model.Article;
import com.pandiandcode.xariti.view.MyTextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import it.sephiroth.android.library.widget.HListView;


public class ArticleListFragment extends Fragment {

    private ArticleListAdapter mAdapter;
    private HListView mHListView;
    private ListView mListView;
    private ImageView mFlange;
    private ImageView mArticleImage;
    private ImageView mSpinner;
    private ImageView mArticleBg;
    private MyTextView mSpinnerText;
    private RelativeLayout mArticleLayout;
    private RelativeLayout mImageArticleLayout;
    private int imageHeight;
    private int imageWidth;
    private int articlePos;
    private int totalArticles;
    private boolean isBackground;
    private boolean isMoving;
    private float oldX;
    private float oldY;

    private GestureDetector mGestureDetector;

    private Activity activity;

    private ArrayList<Article> mArticles;

    private int mCharityID;

    private StatusListener mStatusListener;

    public ArticleListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {

        View mView = inflater.inflate( R.layout.article_list, container, false );

        mArticleLayout = ( RelativeLayout ) getActivity().findViewById( R.id.articles_layout );
        mImageArticleLayout = ( RelativeLayout ) getActivity().findViewById( R.id.article_detail_layout );
        mArticleImage = ( ImageView ) getActivity().findViewById( R.id.article_image );
        mArticleBg = ( ImageView ) getActivity().findViewById( R.id.article_bg );
        mSpinner = ( ImageView ) getActivity().findViewById( R.id.spinner );
        mSpinnerText = (MyTextView) getActivity().findViewById(R.id.spinner_text);
        mFlange = ( ImageView ) mView.findViewById( R.id.flange );

        Bundle args = getArguments();

        if ( args != null )
            mCharityID = args.getInt( "charityID" );

        mArticles = DataController.getCharity( mCharityID, getActivity().getApplicationContext() ).getArticles();
        mImageArticleLayout.setBackgroundColor(getResources().getColor(R.color.green));
        mArticleBg.setImageDrawable(getResources().getDrawable(R.drawable.holder));

        articlePos = 0;
        totalArticles = mArticles.size();

        imageHeight = 0;
        imageWidth = 0;

        setListAdapters( mView );

        return mView;

    }

    @Override
    public void onStart() {
        super.onStart();

        setArticleImageSizes();
        updateArticleImage();
        setGestureArticleImageDetector();
        setOnTouchFlange();

    }

    @Override
    public void onAttach( Activity activity ) {

        super.onAttach( activity );
        this.activity = activity;
        this.mStatusListener = (StatusListener) activity;

    }

    public void setActivityStatus( int status ) {

        this.mStatusListener.setStatus( status );

    }

    public void setListAdapters( View mView ){

        if ( ViewController.isLandscape(getActivity()) ) {

            mListView = ( ListView ) mView.findViewById( R.id.article_listv );
            mAdapter = new ArticleListAdapter( getActivity().getApplicationContext(), mArticles );
            mListView.setAdapter( mAdapter );
            mListView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
                    articlePos = position;
                    updateArticleImage();
                }
            });

        } else {

            mHListView = ( HListView ) mView.findViewById( R.id.article_listv );
            mAdapter = new ArticleListAdapter( getActivity().getApplicationContext(), mArticles );
            mHListView.setAdapter( mAdapter );
            mHListView.setOnItemClickListener( new it.sephiroth.android.library.widget.AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick( it.sephiroth.android.library.widget.AdapterView<?> adapterView, View view, int position, long l ) {
                    articlePos = position;
                    updateArticleImage();
                }
            });
        }
    }

    public void updateArticleImage(){

        mSpinner.setVisibility(View.VISIBLE);
        mSpinnerText.setVisibility(View.VISIBLE);
        Animation animRotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        mSpinner.startAnimation(animRotate);

        int imageW = (mArticles.get(articlePos).getImageW()<imageWidth) ? mArticles.get(articlePos).getImageW() : imageWidth;
        int imageH = (mArticles.get(articlePos).getImageH()<imageHeight) ? mArticles.get(articlePos).getImageH() : imageHeight;

        isBackground = mArticles.get(articlePos).getImageW()==imageW || mArticles.get(articlePos).getImageH()==imageHeight;
        Picasso.with(getActivity().getApplicationContext())
                .load(NetworkController.IMG_URL + mArticles.get(articlePos).getImage() + "&h=" + imageH + "&w=" + imageW + "&q=100")
                .error(R.drawable.holder)
                .into( mArticleImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        onFinishImageLoad();
                    }

                    @Override
                    public void onError() {
                        onFinishImageLoad();
                    }
                });

    }

    public void onFinishImageLoad(){
        mSpinner.setVisibility(View.INVISIBLE);
        mSpinnerText.setVisibility(View.INVISIBLE);
        if(getActivity()!=null){
            getActivity().runOnUiThread(new Runnable() {

                public void run() {

                    BitmapDrawable bitmap = (BitmapDrawable) mArticleImage.getDrawable();
                    mArticleBg.setImageBitmap(ViewController.fastblur(bitmap.getBitmap(),25));

                }
            });

        }

    }
    public void setArticleImageSizes(){

       if ( ViewController.isLandscape( getActivity() ) ) {

           imageHeight = ViewController.getHeightScreen( getActivity() ) - ViewController.getStatusBarHeight(getActivity());
           imageWidth = ViewController.getWidthScreen( getActivity() ) - (int) ViewController.dpiToPixel( getActivity(), ( ViewController.isSmallScreen( getActivity() ) ) ? 170 : 220 );

       }else{

           imageHeight = ViewController.getHeightScreen( getActivity() ) - (int) ViewController.dpiToPixel( getActivity(), ( ViewController.isSmallScreen( getActivity() ) ) ? 230 : 280 );
           imageWidth = ViewController.getWidthScreen( getActivity() );

       }

    }

    public void setGestureArticleImageDetector() {

        mGestureDetector = new GestureDetector( getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onDown( MotionEvent e ) {
                        return true;
                    }

            @Override
            public boolean onFling( MotionEvent e1, MotionEvent e2, float velocityX, float velocityY ) {

                if ( velocityX > 0 )
                    articlePos = ( articlePos == 0 ) ? totalArticles - 1 : articlePos - 1;

                else
                    articlePos = ( articlePos == ( totalArticles - 1 ) ) ? 0 : articlePos + 1;

                updateArticleImage();

                return true;

            }

        });

        mImageArticleLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return mGestureDetector.onTouchEvent( event );

            }
        });
    }

    public void setOnTouchFlange() {

        mFlange.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                float offset = ViewController.dpiToPixel( getActivity(), 30 );
                float articleListSize = ViewController.dpiToPixel( getActivity(), ( ViewController.isSmallScreen( getActivity() ) ) ? 170 : 220);
                float eventX = event.getRawX() - offset;
                float eventY = event.getRawY() - offset;
                float maxHeight = ViewController.getHeightScreen(getActivity()) - articleListSize;
                float maxWidth = ViewController.getWidthScreen( getActivity()) - articleListSize;
                float maxBound = maxHeight / 2;
                float maxBoundLandscape = maxWidth / 2;
                RelativeLayout rlXarityLogo = ( RelativeLayout ) getActivity().findViewById( R.id.xarity_actionbar );

                switch ( event.getAction() ) {

                    case MotionEvent.ACTION_DOWN:

                        isMoving = false;
                        oldX = eventX;
                        oldY = eventY;

                        if ( ( mImageArticleLayout.getHeight() < articleListSize ) ||
                                ( mImageArticleLayout.getWidth() < articleListSize ) ) {

                            mImageArticleLayout.getLayoutParams().width = imageWidth;
                            mImageArticleLayout.getLayoutParams().height = imageHeight;

                            if ( ViewController.isLandscape( getActivity() ) )
                                mImageArticleLayout.setX( eventX + articleListSize );

                            else
                                mImageArticleLayout.setY( eventY + articleListSize + offset * 2 );

                            mImageArticleLayout.requestLayout();
                            mImageArticleLayout.invalidate();

                        }

                        break;

                    case MotionEvent.ACTION_MOVE:

                        if ( mImageArticleLayout.getVisibility() == View.GONE )  mImageArticleLayout.setVisibility( View.VISIBLE );
                        if ( ViewController.isLandscape(getActivity()) ) {

                            if ( ( eventX > 0 ) && ( eventX < maxWidth - offset / 2 ) ) {

                                mArticleLayout.setX( eventX );
                                mImageArticleLayout.setX( eventX + articleListSize );
                                if ( !rlXarityLogo.getTag().equals( "inactive" ) && !isInTwoPanels() )
                                    rlXarityLogo.setAlpha(0);
                            }

                        } else {

                            if ( ( eventY > 0 ) && ( eventY < maxHeight - offset / 2 ) ) {

                                mArticleLayout.setY( eventY );
                                mImageArticleLayout.setY( eventY + articleListSize );

                            }

                        }

                        mFlange.invalidate();

                        break;

                    case MotionEvent.ACTION_UP:

                        if ( mImageArticleLayout.getVisibility() == View.GONE )  mImageArticleLayout.setVisibility( View.VISIBLE );
                        if ( ViewController.isLandscape( getActivity() ) ) {

                            // find is the up action was after moving the view or just tapping it
                            isMoving = !( ( eventX - oldX ) * ( eventX - oldX ) < 10 * 10 );
                            if ( ( eventX > maxBoundLandscape && isMoving ) || ( eventX < maxBoundLandscape && !isMoving ) ) {

                                setActivityStatus( XaritiesViewerActivity.XARITY_STATUS );

                                mArticleLayout.animate().translationX(0);
                                mImageArticleLayout.animate().translationX( imageWidth );
                                mFlange.animate().rotation(0);
                                setActivityStatus( XaritiesViewerActivity.XARITY_STATUS );
                                if ( !rlXarityLogo.getTag().equals( "inactive" ) )
                                    rlXarityLogo.animate().alpha(1);


                            } else {

                                mArticleLayout.animate().translationX( - maxWidth );
                                mImageArticleLayout.animate().translationX( 0 );
                                mFlange.animate().rotation( 180 );

                                setActivityStatus( XaritiesViewerActivity.ARTICLE_STATUS );
                                
                            }

                        } else {

                            // find is the up action was after moving the view or just tapping it
                            isMoving = !( ( eventY - oldY ) * ( eventY - oldY ) < 10 * 10 );
                            if ( ( eventY > maxBound && isMoving ) || ( eventY < maxBound && !isMoving ) ) {

                                mArticleLayout.animate().translationY(0);
                                mImageArticleLayout.animate().translationY( imageHeight );
                                if (rlXarityLogo.getTag().equals("inactive"))
                                    rlXarityLogo.animate().alpha(0);
                                mFlange.animate().rotation(0);

                                setActivityStatus(XaritiesViewerActivity.XARITY_STATUS);

                            } else {

                                mArticleLayout.animate().translationY( - maxHeight + offset * 2 + ViewController.dpiToPixel( getActivity(), 4 ) );
                                mImageArticleLayout.animate().translationY(0);
                                if( !isInTwoPanels() ) rlXarityLogo.animate().alpha(1);
                                mFlange.animate().rotation(180);

                                setActivityStatus( XaritiesViewerActivity.ARTICLE_STATUS );

                            }

                        }

                        mFlange.invalidate();

                        break;

                    default:
                        return false;

                }

                return true;

            }

        });

    }

    public boolean isInTwoPanels(){
        return mStatusListener.isInTwoPaneMode();
    }
}
