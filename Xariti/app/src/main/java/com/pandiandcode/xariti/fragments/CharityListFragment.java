package com.pandiandcode.xariti.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pandiandcode.xariti.Adapters.XaritiListAdapter;
import com.pandiandcode.xariti.Listeners.CharitySelectedListener;
import com.pandiandcode.xariti.R;
import com.pandiandcode.xariti.controller.DataController;
import com.pandiandcode.xariti.model.CharityDistance;

import java.util.ArrayList;


public class CharityListFragment extends Fragment{

    private static final double mDistance = 3;

    private ListView mListView;
    private XaritiListAdapter mListAdapter;
    private CharitySelectedListener mCharityListener;

    public CharityListFragment() {}

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
    }


    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {

        View mView = inflater.inflate( R.layout.charity_list, container, false );

        mListView = ( ListView )mView.findViewById( R.id.charity_listv );

        SharedPreferences prefs = getActivity().getSharedPreferences( DataController.PACKAGE, Context.MODE_PRIVATE );
        double mLong = Double.parseDouble( prefs.getString( "currentLong", "-0.1242057" ) );
        double mLat = Double.parseDouble( prefs.getString( "currentLat", "51.5219824" ) );

        ArrayList<CharityDistance> mCharities = DataController.getCharities( mLat, mLong, mDistance, false, getActivity() );

        mListAdapter = new XaritiListAdapter( getActivity(), mCharities );
        mListView.setAdapter( mListAdapter );

        mListView.setOnItemClickListener( new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {

                int charityID = ( ( CharityDistance ) mListAdapter.getItem( position ) ).getCharity().getCharityID();
                try {

                    mCharityListener.onCharityPicked( charityID );

                } catch ( ClassCastException cce ) {}

            }

        });

        return mView;

    }

    @Override
    public void onAttach( Activity activity ) {

        super.onAttach( activity );
        this.mCharityListener = ( CharitySelectedListener ) activity;

    }

    @Override
    public void onDetach() { super.onDetach(); }

    @Override
    public void onActivityCreated( Bundle savedInstanceState ) { super.onActivityCreated( savedInstanceState ); }

    /**
     * Update CharityDistance ListView with the new CharityDistance objects and the new location
     * @param lat latitude of the current location or search
     * @param longi longitude of the current location or search
     */
    public void updateList( Double lat, double longi ) {

        ArrayList<CharityDistance> mCharities = DataController.getCharities( lat, longi, mDistance, false, getActivity() );
        mListAdapter.update( mCharities );

    }


}
