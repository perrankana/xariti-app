package com.pandiandcode.xariti.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pandiandcode.xariti.Listeners.CharitySelectedListener;
import com.pandiandcode.xariti.R;
import com.pandiandcode.xariti.controller.DataController;
import com.pandiandcode.xariti.model.CharityDistance;

import java.util.ArrayList;
import java.util.HashMap;


public class CharityMapFragment extends Fragment {

    private static final double mDistance = 3;

    private MapView mapView;
    private GoogleMap mGmap;
    private ImageButton mBtnLocation;
    private ImageView mSnapshot;

    private HashMap<String,Integer> markerCharities;
    private ArrayList<CharityDistance> mCharities;
    private int mCharityID;
    private CharitySelectedListener mCharityListener;

    @Override
    public void onCreate( Bundle savedInstanceState ) { super.onCreate( savedInstanceState );}

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {

        View v = inflater.inflate( R.layout.charity_map, container, false );

        mSnapshot = ( ImageView )v.findViewById( R.id.snapshot );
        mBtnLocation = ( ImageButton )v.findViewById( R.id.btn_position );
        mapView = ( MapView ) v.findViewById( R.id.mapview );

        mBtnLocation.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                mCharityListener.onSearchLocation();
            }
        });

        markerCharities = new HashMap<>();
        mCharityID = 0;

        Bundle args = getArguments();
        if ( args != null )
            mCharityID = args.getInt( "charityID" );


        mapView.onCreate( savedInstanceState );

        mGmap = mapView.getMap();

        MapsInitializer.initialize(getActivity());

        updateMap();

        return v;

    }

    @Override
    public void onResume() {

        super.onResume();
        mapView.onResume();

    }

    @Override
    public void onStart() { super.onStart(); }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if ( mapView != null )
            mapView.onDestroy();

    }

    @Override
    public void onAttach( Activity activity ) {

        super.onAttach( activity );
        mCharityListener = (CharitySelectedListener) activity;

    }

    @Override
    public void onLowMemory() {

        super.onLowMemory();
        mapView.onLowMemory();

    }

    public void updateMap(){

        SharedPreferences prefs = getActivity().getSharedPreferences( DataController.PACKAGE, Context.MODE_PRIVATE );
        double mLong = Double.parseDouble( prefs.getString( "currentLong", "-0.1242057" ) );
        double mLat = Double.parseDouble( prefs.getString( "currentLat", "51.5219824" ) );
        LatLng latLng = new LatLng( mLat,mLong );
        LatLng mCenter = new LatLng( mLat,mLong );

        mCharities = DataController.getCharities( latLng.latitude, latLng.longitude, mDistance, false, getActivity() );

        if( mGmap != null )
            mGmap.clear();

        for ( CharityDistance charity : mCharities ) {

            Marker mMarker = mGmap.addMarker( new MarkerOptions()
                        .position( new LatLng( charity.getCharity().getLatitude(), charity.getCharity().getLongitude() ) )
                        .title( charity.getCharity().getTitle() )
                        .snippet( charity.getCharity().getAddress() )
                        .icon( BitmapDescriptorFactory.fromResource( R.drawable.ic_map_marker ) ) );

            if ( mCharityID == charity.getCharity().getCharityID() ) {

                mMarker.showInfoWindow();
                mCenter = new LatLng( charity.getCharity().getLatitude(), charity.getCharity().getLongitude() );

            }

            markerCharities.put( mMarker.getId(), charity.getCharity().getCharityID() );

        }

        mGmap.addMarker( new MarkerOptions()
                .title( mCharityListener.isSearch() ? "Your search" : "You are here" )
                .position( latLng ).icon( BitmapDescriptorFactory.fromResource( R.drawable.ic_location ) ) );

        mGmap.setOnInfoWindowClickListener( new GoogleMap.OnInfoWindowClickListener() {

                @Override
                public void onInfoWindowClick( Marker marker ) {

               if ( markerCharities.containsKey( marker.getId() ) ) {

                    int charityID = markerCharities.get( marker.getId() );

                    try {

                        mCharityListener.onCharityPicked( charityID );

                    } catch ( ClassCastException cce ) {}

               }

            }

        });

        mGmap.setInfoWindowAdapter( new GoogleMap.InfoWindowAdapter() {

                @Override
                public View getInfoWindow( Marker marker ) {

                    return null;

                }

                @Override
                public View getInfoContents( Marker marker ) {

                    View v = getActivity().getLayoutInflater().inflate( R.layout.info_window_layout, null );

                    String gmTitle = marker.getTitle();
                    String gmDescription = marker.getSnippet();

                    TextView mTitle = ( TextView ) v.findViewById( R.id.gm_title );
                    TextView mDescription = ( TextView ) v.findViewById( R.id.gm_description );

                    mTitle.setText( gmTitle );
                    mDescription.setText( gmDescription );

                    return v;

                }

            });

        mGmap.moveCamera( CameraUpdateFactory.newLatLngZoom( mCenter, 10 ) );

        mGmap.animateCamera( CameraUpdateFactory.zoomTo(12), 2000, null );

        mGmap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            public void onMapLoaded() {
                if ( mSnapshot != null )
                    getSnapShot();

            }
        });
    }

    public void getSnapShot(){

        if(mGmap!=null) {
            mGmap.snapshot( new GoogleMap.SnapshotReadyCallback() {
                @Override
                public void onSnapshotReady( Bitmap bitmap ) {

                    mSnapshot.setBackgroundDrawable( new BitmapDrawable( getResources(), bitmap ) );
                }
            });
        }
    }
}

