package com.pandiandcode.xariti.fragments;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.pandiandcode.xariti.R;

/**
 * Created by Perrankana on 11/03/15.
 */
public class InfoFragment extends Fragment {

    private ImageButton mInfoInstagram;
    private ImageButton mInfoTwitter;
    private ImageButton mInfoFacebook;
    private ImageButton mInfoGoogle;
    private ImageButton mInfoEmail;
    private ImageView mIVLicense;
    private ImageButton mInfoPandi;

    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
    }


    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {

        View mView = inflater.inflate( R.layout.info_fragment, container, false );

        mInfoInstagram = ( ImageButton ) mView.findViewById( R.id.info_instagram );
        mInfoTwitter = ( ImageButton ) mView.findViewById( R.id.info_twitter );
        mInfoFacebook = ( ImageButton ) mView.findViewById( R.id.info_facebook );
        mInfoGoogle = ( ImageButton ) mView.findViewById( R.id.info_google );
        mInfoEmail = ( ImageButton ) mView.findViewById( R.id.info_email );
        mInfoPandi = ( ImageButton ) mView.findViewById( R.id.info_pandi );
        mIVLicense = ( ImageView ) mView.findViewById( R.id.iv_license );

        return mView;
    }

    @Override
    public void onAttach( Activity activity ) { super.onAttach( activity ); }

    @Override
    public void onDetach() { super.onDetach(); }

    @Override
    public void onStart() {

        super.onStart();
        mInfoInstagram.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {

                try {

                    Intent iIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.instagram.android");
                    iIntent.setComponent( new ComponentName( "com.instagram.android", "com.instagram.android.activity.UrlHandlerActivity") );
                    iIntent.setData( Uri.parse("http://instagram.com/_u/xariti_pc/") );
                    startActivity( iIntent );
                    Toast.makeText(getActivity(), "Launching Instagram app", Toast.LENGTH_LONG).show();

                } catch ( Exception e ) {

                    startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse("https://instagram.com/xariti_pc" ) ) );
                    Toast.makeText(getActivity(),"Launching Browser",Toast.LENGTH_LONG).show();

                }
            }
        });

        mInfoTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=xariti_pc" ) );
                    startActivity( intent );
                    Toast.makeText(getActivity(),"Launching Twitter app",Toast.LENGTH_LONG).show();

                } catch ( Exception e ) {

                    startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "https://twitter.com/#!/xariti_pc") ) );
                    Toast.makeText(getActivity(),"Launching Browser",Toast.LENGTH_LONG).show();

                }

            }
        });

        mInfoFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    String facebookScheme = "fb://page/1387180738266502";
                    Intent facebookIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( facebookScheme ) );
                    startActivity( facebookIntent );
                    Toast.makeText(getActivity(),"Launching Facebook app, it'll take a while...",Toast.LENGTH_LONG).show();

                } catch ( ActivityNotFoundException e ) {

                    Intent facebookIntent = new Intent( Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/1387180738266502" ) );
                    startActivity( facebookIntent );
                    Toast.makeText(getActivity(),"Launching Browser",Toast.LENGTH_LONG).show();

                }
            }
        });

        mInfoGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "https://plus.google.com/" + "104617570246953443113" + "/posts") ) );

            }
        });

        mInfoEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent emailIntent = new Intent( Intent.ACTION_SENDTO, Uri.fromParts( "mailto","info@xariti.pandiandcode.com", null ) );
                emailIntent.putExtra( Intent.EXTRA_SUBJECT, "Enquiry from Xariti APP" );
                startActivity(Intent.createChooser( emailIntent, "Send email..." ) );
            }
        });

        mInfoPandi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( "http://pandiandcode.com" ) );
                startActivity( intent );
                Toast.makeText(getActivity(),"Launching Browser",Toast.LENGTH_LONG).show();

            }
        });

        mIVLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse("http://creativecommons.org/licenses/by-nc-sa/4.0/" ) );
                startActivity( intent );

            }
        });
    }

    @Override
    public void onActivityCreated( Bundle savedInstanceState ) { super.onActivityCreated( savedInstanceState ); }

}
