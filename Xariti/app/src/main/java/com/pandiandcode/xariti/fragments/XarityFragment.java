package com.pandiandcode.xariti.fragments;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.pandiandcode.xariti.Listeners.ScrollViewListener;
import com.pandiandcode.xariti.Listeners.StatusListener;
import com.pandiandcode.xariti.R;
import com.pandiandcode.xariti.XaritiesViewerActivity;
import com.pandiandcode.xariti.controller.DataController;
import com.pandiandcode.xariti.controller.NetworkController;
import com.pandiandcode.xariti.controller.ViewController;
import com.pandiandcode.xariti.model.ChaBrand;
import com.pandiandcode.xariti.model.Charity;
import com.pandiandcode.xariti.model.Link;
import com.pandiandcode.xariti.view.MyScrollView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class XarityFragment extends Fragment {

    private Activity activity;
    private int mCharityID;
    private Charity mCharity;
    private TextView mTitle;
    private TextView mDescription;
    private ImageView mLogo;
    private ImageView mLogoABar;
    private String mUrlLogo;
    private ImageButton mInstagram;
    private ImageButton mFacebook;
    private ImageButton mTwitter;
    private ImageButton mGoogleplus;
    private ImageButton mEmail;
    private ImageButton mPhone;
    private MyScrollView mScrollView;
    private ScrollView mSocialLayout;
    private StatusListener mStatusListener;

    public XarityFragment() {}

    @Override
    public void onActivityCreated( Bundle savedInstanceState ) { super.onActivityCreated( savedInstanceState ); }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
    }


    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {

        View mView = inflater.inflate( R.layout.fragment_xartity, container, false );

        mTitle = ( TextView ) mView.findViewById( R.id.xtitle );
        mDescription = ( TextView ) mView.findViewById( R.id.xdescription );
        mLogo = ( ImageView ) mView.findViewById( R.id.xlogo );
        mLogoABar = ( ImageView ) getActivity().findViewById( R.id.xarity_logo_single_view );
        mInstagram = ( ImageButton ) mView.findViewById( R.id.intagram_btn );
        mFacebook = ( ImageButton ) mView.findViewById( R.id.facebook_btn );
        mTwitter = ( ImageButton ) mView.findViewById( R.id.twitter_btn );
        mGoogleplus = ( ImageButton ) mView.findViewById( R.id.googleplus_btn );
        mEmail = (ImageButton) mView.findViewById( R.id.email_btn );
        mPhone = (ImageButton) mView.findViewById( R.id.phone_btn );
        mScrollView = ( MyScrollView ) mView.findViewById( R.id.scrollView );
        mSocialLayout = (ScrollView) mView.findViewById( R.id.social_layout );

        return mView;

    }

    @Override
    public void onAttach( Activity activity ) {

        super.onAttach( activity );
        this.activity = activity;
        this.mStatusListener = (StatusListener) activity;

    }

    @Override
    public void onDetach() { super.onDetach(); }

    @Override
    public void onStart() {

        super.onStart();

        Bundle args = getArguments();
        if ( args != null ) {

            mCharityID = args.getInt("charityID");
            mCharity = DataController.getCharity( mCharityID, getActivity().getApplicationContext() );
            RelativeLayout.LayoutParams params = ( RelativeLayout.LayoutParams ) mSocialLayout.getLayoutParams();

            if ( !ViewController.isLandscape( getActivity() ) ) {

                if ( mCharity.getArticles().size() != 0 ) {

                    int margin = (int) ViewController.dpiToPixel( getActivity(), ( ViewController.isSmallScreen( getActivity() ) ) ? 150 : 200 );
                    params.setMargins( 0, 0, 0, margin );

                } else {

                    params.setMargins( 0, 0, 0, 0 );

                }

            } else {

                params.setMargins( 0, 0, 0, 0 );

            }

            mSocialLayout.setLayoutParams( params );
            mSocialLayout.requestLayout();
            mScrollView.requestLayout();

            if ( mCharity != null ) {

                mTitle.setText( mCharity.getTitle() );
                mDescription.setText( mCharity.getDescription() );

                ChaBrand mChaBrand = DataController.getChaBrand( mCharity.getChaBrandID(), getActivity().getApplicationContext() );
                mUrlLogo = "";
                if ( mChaBrand != null )
                    mUrlLogo = mChaBrand.getImage();

                if ( !mUrlLogo.equals("") ) {

                    int imageSize = (int) ViewController.dpiToPixel( getActivity(), 78 );
                    Picasso.with(getActivity().getApplicationContext())
                            .load( NetworkController.IMG_URL + mUrlLogo + "&h=" + imageSize + "&w=" + imageSize + "&crop-to-fit&q=100" )
                            .resize(imageSize, imageSize)
                            .centerCrop()
                            .placeholder( R.drawable.holder )
                            .error( R.drawable.holder ).into( mLogo );
                    int imageABSize = (int) ViewController.dpiToPixel( getActivity(), 50 );
                    Picasso.with(getActivity().getApplicationContext())
                            .load( NetworkController.IMG_URL + mUrlLogo + "&h=" + imageABSize + "&w=" + imageABSize + "&crop-to-fit&q=100" )
                            .resize(imageABSize, imageABSize)
                            .centerCrop()
                            .placeholder( R.drawable.holder )
                            .error( R.drawable.holder ).into( mLogoABar );

                }

                mScrollView.setScrollViewListener( new ScrollViewListener() {

                    @Override
                    public void onScrollChanged( MyScrollView scrollView, int x, int y, int oldx, int oldy ) {

                        int actionBarHeight = (int) ViewController.dpiToPixel( getActivity(), 60 );
                        RelativeLayout rlXarityLogo = ( RelativeLayout )activity.findViewById( R.id.xarity_actionbar );

                        if ( ( ( ( XaritiesViewerActivity )activity ).getStatus() == XaritiesViewerActivity.XARITY_STATUS ) ||
                                isInTwoPanels() ) {

                            if ( oldy < y ) {

                                //scroll down

                                if ( y < actionBarHeight )
                                    rlXarityLogo.setAlpha( 1 - ( ( float )actionBarHeight - y ) / actionBarHeight );

                                else {

                                    rlXarityLogo.setAlpha(1);
                                    rlXarityLogo.setTag("active");

                                }

                            } else {

                                // scroll up

                                if ( y < actionBarHeight ) {

                                    rlXarityLogo.setAlpha( 1 - ( ( float ) actionBarHeight - y ) / actionBarHeight );
                                    rlXarityLogo.setTag("inactive");

                                }

                            }

                        }

                    }

                });

                ArrayList<Link> links = mCharity.getLinks();

                for ( final Link link : links ) {

                    switch ( link.getTitle() ) {

                        case "instagram":

                            mInstagram.setVisibility( View.VISIBLE );
                            mInstagram.setOnClickListener( new View.OnClickListener() {

                                @Override
                                public void onClick( View v ) {

                                    if ( getActivityStatus() == XaritiesViewerActivity.XARITY_STATUS ) {

                                        try {

                                            Intent iIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.instagram.android");
                                            iIntent.setComponent( new ComponentName( "com.instagram.android", "com.instagram.android.activity.UrlHandlerActivity") );
                                            iIntent.setData( Uri.parse("http://instagram.com/_u/" + link.getLink() + "/") );
                                            startActivity( iIntent );
                                            Toast.makeText(getActivity(),"Launching Instagram app",Toast.LENGTH_LONG).show();

                                        } catch ( Exception e ) {

                                            startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse("https://instagram.com/" + link.getLink() ) ) );
                                            Toast.makeText(getActivity(),"Launching Browser",Toast.LENGTH_LONG).show();

                                        }

                                    }

                                }

                            });

                            break;

                        case "facebook":

                            mFacebook.setVisibility( View.VISIBLE );
                            mFacebook.setOnClickListener( new View.OnClickListener() {

                                @Override
                                public void onClick( View v ) {

                                    if ( getActivityStatus() == XaritiesViewerActivity.XARITY_STATUS ) {

                                        try {

                                            String facebookScheme = "fb://page/" + link.getLink();
                                            Intent facebookIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( facebookScheme ) );
                                            startActivity( facebookIntent );
                                            Toast.makeText(getActivity(),"Launching Facebook app, it'll take a while...",Toast.LENGTH_LONG).show();

                                        } catch ( ActivityNotFoundException e ) {

                                            Intent facebookIntent = new Intent( Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/profile.php?id=" + link.getLink() ) );
                                            startActivity( facebookIntent );
                                            Toast.makeText(getActivity(),"Launching Browser",Toast.LENGTH_LONG).show();

                                        }

                                    }

                                }

                            });

                            break;

                        case "twitter":

                            mTwitter.setVisibility( View.VISIBLE );
                            mTwitter.setOnClickListener( new View.OnClickListener() {

                                @Override
                                public void onClick( View v ) {

                                    if ( getActivityStatus() == XaritiesViewerActivity.XARITY_STATUS ) {

                                        try {

                                            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + link.getLink() ) );
                                            startActivity( intent );
                                            Toast.makeText(getActivity(),"Launching Twitter app",Toast.LENGTH_LONG).show();

                                        } catch ( Exception e ) {

                                            startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "https://twitter.com/#!/" + link.getLink() ) ) );
                                            Toast.makeText(getActivity(),"Launching Browser",Toast.LENGTH_LONG).show();

                                        }

                                    }

                                }

                            });

                            break;

                        case "google":

                            mGoogleplus.setVisibility( View.VISIBLE );
                            mGoogleplus.setOnClickListener( new View.OnClickListener() {

                                @Override
                                public void onClick( View v ) {

                                    if ( getActivityStatus() == XaritiesViewerActivity.XARITY_STATUS ) {

                                        startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "https://plus.google.com/" + link.getLink() + "/posts") ) );

                                    }

                                }

                            });

                            break;

                        case "email":

                            mEmail.setVisibility( View.VISIBLE);
                            mEmail.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if( getActivityStatus() == XaritiesViewerActivity.XARITY_STATUS ){

                                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts( "mailto",link.getLink(), null));
                                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Enquiry from Xariti APP");
                                        startActivity(Intent.createChooser(emailIntent, "Send email..."));

                                    }

                                }
                            });

                            break;

                        case "phone":

                            mPhone.setVisibility( View.VISIBLE);
                            mPhone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if( getActivityStatus() == XaritiesViewerActivity.XARITY_STATUS ){

                                        String uri = "tel:" + link.getLink() ;
                                        Intent intent = new Intent(Intent.ACTION_DIAL);
                                        intent.setData(Uri.parse(uri));
                                        startActivity(intent);

                                    }

                                }
                            });

                            break;

                        default:

                    }

                }

            }

        }

    }


    public int getActivityStatus() {

        return mStatusListener.getStatus();

    }

    public boolean isInTwoPanels(){

        return mStatusListener.isInTwoPaneMode();
    }
}
