package com.pandiandcode.xariti.model;

import java.util.Date;

/**
 * Created by Perrankana on 06/01/15.
 */
public class Article {
    private int articleID;
    private String title;
    private String description;
    private String image;
    private Date date;
    private int charityID;
    private int imageW;
    private int imageH;

    public Article(){}

    public Article(int articleID, String title, String description, String image, Date date, int charityID, int imageW, int imageH){
        this.articleID = articleID;
        this.description = description;
        this.title = title;
        this.image = image;
        this.date = date;
        this.charityID = charityID;
        this.imageW = imageW;
        this.imageH = imageH;
    }

    public int getArticleID() {
        return articleID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getImageW(){ return imageW; }

    public void setImageW( int imageW ){ this.imageW = imageW; }

    public int getImageH(){ return imageH; }

    public void setImageH( int imageH ){ this.imageH = imageH; }

    public int getCharityID() {
        return charityID;
    }

    public void setCharityID(int charityID) {
        this.charityID = charityID;
    }
}
