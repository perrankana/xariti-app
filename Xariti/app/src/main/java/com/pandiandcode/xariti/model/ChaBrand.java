package com.pandiandcode.xariti.model;

/**
 * Created by Perrankana on 06/01/15.
 */
public class ChaBrand {
    private int chaBrandID;
    private String title;
    private String description;
    private String image;
    private String website;

    public ChaBrand(){}

    public ChaBrand(int chaBrandID, String title, String description, String image, String website){
        this.chaBrandID = chaBrandID;
        this.title = title;
        this.description = description;
        this.image = image;
        this.website = website;
    }

    public ChaBrand(String title, String description, String image, String website){
        this.title = title;
        this.description = description;
        this.image = image;
        this.website = website;
    }

    public int getChaBrandID() {
        return chaBrandID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
