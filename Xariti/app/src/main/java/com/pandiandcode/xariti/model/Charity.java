package com.pandiandcode.xariti.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Perrankana on 06/01/15.
 */
public class Charity {
    private int charityID;
    private String title;
    private String description;
    private String address;
    private String postcode;
    private double latitude;
    private double longitude;
    private Date date;
    private ArrayList<Article> articles;
    private ArrayList<Link> links;
    private int chaBrandID;

    public Charity(){}

    public Charity(int charityID, String title, String description, String address, String postcode,
            double latitude, double longitude, Date date, ArrayList<Article> articles, ArrayList<Link> links, int chaBrandID){
        this.charityID = charityID;
        this.title = title;
        this.description = description;
        this.address = address;
        this.postcode = postcode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.articles = articles;
        this.links = links;
        this.chaBrandID = chaBrandID;
    }
    public Charity(int charityID, String title, String description, String address, String postcode,
                   double latitude, double longitude, Date date, int chaBrandID){
        this.charityID = charityID;
        this.title = title;
        this.description = description;
        this.address = address;
        this.postcode = postcode;
        this.longitude = longitude;
        this.latitude = latitude;
        this.date = date;
        this.chaBrandID = chaBrandID;
    }

    public boolean addLink(Link newLink){
        return this.links.add(newLink);
    }

    public boolean addArticle(Article newArticle){
        return this.articles.add(newArticle);
    }

    public int getCharityID() {
        return charityID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<Article> articles) {
        this.articles = articles;
    }

    public ArrayList<Link> getLinks() {
        return links;
    }

    public void setLinks(ArrayList<Link> links) {
        this.links = links;
    }

    public int getChaBrandID() {
        return chaBrandID;
    }

    public void setChaBrandID(int chaBrandID) {
        this.chaBrandID = chaBrandID;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
