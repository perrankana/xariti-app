package com.pandiandcode.xariti.model;


public class CharityDistance implements Comparable{

    private Charity charity;
    private double distance;

    public CharityDistance(Charity charity, double distance){
        this.charity = charity;
        this.distance = distance;
    }

    public Charity getCharity() {
        return charity;
    }


    public Double getDistance() {
        return distance;
    }

    public int compareTo(Object another) {
        if(this.distance > ((CharityDistance)another).getDistance()){
            return 1;
        }
        return -1;
    }


}
