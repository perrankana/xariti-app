package com.pandiandcode.xariti.model;

/**
 * Created by Perrankana on 06/01/15.
 */
public class Link {
    private int linkID;
    private String title;
    private String link;
    private int charityID;

    public Link(){}

    public Link(int linkID, String title, String link, int charityID){
        this.linkID = linkID;
        this.title = title;
        this.link = link;
        this.charityID = charityID;
    }

    public int getLinkID() {
        return linkID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getCharityID() {
        return charityID;
    }

    public void setCharityID(int charityID) {
        this.charityID = charityID;
    }
}
