package com.pandiandcode.xariti.modelDAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.pandiandcode.xariti.model.Article;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Perrankana on 06/01/15.
 */
public class ArticleDAO {

    private DataBaseOpenHelper conexion;
    private SQLiteDatabase db;
    private Context context;

    public ArticleDAO(Context context){
        this.context = context;
        try{
            this.conexion= DataBaseOpenHelper.getHelper(context);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean insert(Article article){

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try{
            this.db=conexion.getWritableDatabase();

            ContentValues nr=new ContentValues();
            nr.put("articleID", article.getArticleID());
            nr.put("title", article.getTitle());
            nr.put("description", article.getDescription());
            nr.put("image", article.getImage());
            nr.put("date", format.format(article.getDate()));
            nr.put("charityID", article.getCharityID());
            nr.put("imageW", article.getImageW());
            nr.put("imageH", article.getImageH());

            int status=(int) this.db.insert("article", null, nr);
            db.close();
            conexion.close();

            return (status!=-1);

        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean delete(int articleID){

        try{
            this.db= this.conexion.getWritableDatabase();

            String [] args=new String[] {articleID+""};

            int status=this.db.delete("article","articleID=?",args);
            db.close();
            conexion.close();

            return (status!=0);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean deleteAll(int charityID){

        try{
            this.db= this.conexion.getWritableDatabase();

            String [] args=new String[] {charityID+""};

            int status=this.db.delete("article","charityID=?",args);
            db.close();
            conexion.close();

            return (status!=0);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean updateArticle(Article article){

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try{

            this.db= this.conexion.getWritableDatabase();

            String [] args=new String[] {String.valueOf(article.getArticleID())};

            ContentValues cv=new ContentValues();
            cv.put("title", article.getTitle());
            cv.put("description", article.getDescription());
            cv.put("image", article.getImage());
            cv.put("date", format.format(article.getDate()));
            cv.put("imageW", article.getImageW());
            cv.put("imageH", article.getImageH());

            int status=this.db.update("article", cv, "articleID=?", args);

            db.close();
            conexion.close();


            return (status!=0);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public ArrayList<Article> selectArticles(int charityID){

        Cursor cursor=null;
        ArrayList <Article> articles = new ArrayList <Article>();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try{
            String [] campos=new String [] {"articleID","title","description","image","date", "charityID", "imageW", "imageH"};

            String[] args=new String[] {charityID+""};

            this.db= this.conexion.getWritableDatabase();

            cursor=this.db.query("article", campos, "charityID=?", args, null, null, null);

            if(cursor.moveToFirst()){
                do{

                    articles.add(new Article(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), format.parse(cursor.getString(4)), cursor.getInt(5), cursor.getInt(6), cursor.getInt(7)));

                }while(cursor.moveToNext());
            }
            db.close();
            conexion.close();


            return articles;
        }catch(Exception e){
            e.printStackTrace();
            return articles;
        }finally{
            if(cursor!=null) cursor.close();
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean bulk(ArrayList<Article> articles){

        Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try{
            this.db=conexion.getWritableDatabase();

            String sql = "INSERT INTO article VALUES (?,?,?,?,?,?,?,?);";
            SQLiteStatement statement = this.db.compileStatement(sql);
            this.db.beginTransaction();
            for(Article article : articles){
                statement.clearBindings();
                statement.bindLong(1,article.getArticleID());
                statement.bindString(2,article.getTitle());
                statement.bindString(3,article.getDescription());
                statement.bindString(4,article.getImage());
                statement.bindString(5,format.format(article.getDate()));
                statement.bindLong(6,article.getCharityID());
                statement.bindLong(7,article.getImageW());
                statement.bindLong(8,article.getImageH());
                statement.execute();
            }
            db.setTransactionSuccessful();
            db.endTransaction();

            db.close();
            conexion.close();


            return true;

        }catch(Exception e){
            db.endTransaction();
            if(db!=null){this.db.close();}
            if(this.conexion!=null){this.conexion.close();}
            return false;
        }


    }

}
