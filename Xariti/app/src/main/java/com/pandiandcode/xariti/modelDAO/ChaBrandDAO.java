package com.pandiandcode.xariti.modelDAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.pandiandcode.xariti.model.ChaBrand;

import java.util.ArrayList;


/**
 * Created by Perrankana on 06/01/15.
 */
public class ChaBrandDAO {

    private DataBaseOpenHelper conexion;
    private SQLiteDatabase db;
    private Context context;

    public ChaBrandDAO(Context context){
        this.context = context;
        try{
            this.conexion= DataBaseOpenHelper.getHelper(context);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean insert(ChaBrand chaBrand){
        try{
            this.db=conexion.getWritableDatabase();

            ContentValues nr=new ContentValues();
            nr.put("cha_brandID", chaBrand.getChaBrandID());
            nr.put("title", chaBrand.getTitle());
            nr.put("description", chaBrand.getDescription());
            nr.put("image", chaBrand.getImage());
            nr.put("website", chaBrand.getWebsite());

            int status=(int) this.db.insert("chaBrand", null, nr);
            db.close();
            conexion.close();

            return (status!=-1);

        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean delete(int chaBrandID){

        try{
            this.db= this.conexion.getWritableDatabase();

            String [] args=new String[] {chaBrandID+""};

            int status=this.db.delete("chaBrand","cha_brandID=?",args);
            db.close();
            conexion.close();

            return (status!=0);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean updateChaBrand(ChaBrand chaBrand){
        try{

            this.db= this.conexion.getWritableDatabase();

            String [] args=new String[] {String.valueOf(chaBrand.getChaBrandID())};

            ContentValues cv=new ContentValues();
            cv.put("title", chaBrand.getTitle());
            cv.put("description", chaBrand.getDescription());
            cv.put("image", chaBrand.getImage());
            cv.put("website", chaBrand.getWebsite());

            int status=this.db.update("chaBrand", cv, "cha_brandID=?", args);

            db.close();
            conexion.close();


            return (status!=0);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public ChaBrand select(int chaBrandID){

        ChaBrand chaBrand = null;

        Cursor cursor=null;

        try{
            String [] campos=new String [] {"cha_brandID","title","description","image","website"};

            String[] args = new String[]{chaBrandID+""};

            this.db= this.conexion.getWritableDatabase();

            cursor=this.db.query("chaBrand", campos, "cha_brandID=?", args, null, null, null);

            if(cursor.moveToFirst()){
                do{
                    chaBrand = new ChaBrand(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));

                }while(cursor.moveToNext());

            }

        }catch(Exception e){
            e.printStackTrace();
            return chaBrand;
        }finally{
            if(cursor != null)
                cursor.close();
            this.db.close();
            this.conexion.close();
            return chaBrand;
        }

    }

    public ArrayList<ChaBrand> selectAll(){

        ArrayList <ChaBrand> chaBrands = new ArrayList <ChaBrand>();

        Cursor cursor=null;

        try{
            String [] campos=new String [] {"cha_brandID","title","description","image","website"};

            this.db= this.conexion.getWritableDatabase();

            cursor=this.db.query("chaBrand", campos, null, null, null, null, null);

            if(cursor.moveToFirst()){
                do{
                    ChaBrand chaBrand = new ChaBrand(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                    chaBrands.add(chaBrand);

                }while(cursor.moveToNext());

            }

            db.close();
            conexion.close();

            return chaBrands;
        }catch(Exception e){
            e.printStackTrace();
            return chaBrands;
        }finally{
            cursor.close();
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean bulk(ArrayList<ChaBrand> chaBrands){

        try{
            this.db=conexion.getWritableDatabase();

            String sql = "INSERT INTO chaBrand VALUES (?,?,?,?,?);";
            SQLiteStatement statement = this.db.compileStatement(sql);
            this.db.beginTransaction();
            for(ChaBrand chaBrand : chaBrands){
                statement.clearBindings();
                statement.bindLong(1, chaBrand.getChaBrandID());
                statement.bindString(2, chaBrand.getTitle());
                statement.bindString(4, chaBrand.getDescription());
                statement.bindString(3, chaBrand.getImage());
                statement.bindString(5, chaBrand.getWebsite());
                statement.execute();
            }
            db.setTransactionSuccessful();
            db.endTransaction();

            db.close();
            conexion.close();


            return true;

        }catch(Exception e){
            db.endTransaction();
            if(db!=null){this.db.close();}
            if(this.conexion!=null){this.conexion.close();}
            return false;
        }


    }
}
