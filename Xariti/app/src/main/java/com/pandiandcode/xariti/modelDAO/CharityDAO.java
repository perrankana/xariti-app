package com.pandiandcode.xariti.modelDAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.pandiandcode.xariti.model.Charity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Perrankana on 06/01/15.
 */
public class CharityDAO {

    private DataBaseOpenHelper conexion;
    private SQLiteDatabase db;
    private Context context;

    public CharityDAO(Context context){
        this.context = context;
        try{
            this.conexion= DataBaseOpenHelper.getHelper(context);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean insert(Charity charity){

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try{
			this.db=conexion.getWritableDatabase();

			ContentValues nr=new ContentValues();
            nr.put("charityID", charity.getCharityID());
            nr.put("title", charity.getTitle());
            nr.put("description", charity.getDescription());
            nr.put("address", charity.getAddress());
            nr.put("postcode", charity.getPostcode());
            nr.put("latitude", charity.getLatitude());
            nr.put("longitude", charity.getLongitude());
            nr.put("date", format.format(charity.getDate()));
            nr.put("cha_brandID", charity.getChaBrandID());

            int status=(int) this.db.insert("charity", null, nr);
            db.close();
            conexion.close();

            return (status!=-1);

        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean delete(int charityID){

        try{
			this.db= this.conexion.getWritableDatabase();

			String [] args=new String[] {charityID+""};

			int status=this.db.delete("charity","charityID=?",args);
            db.close();
            conexion.close();

            return (status!=0);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public Charity select(int charityID){

        Charity charity = null;

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Cursor cursor=null;

        try{
            String [] campos=new String [] {"charityID","title","description","address","postcode","latitude","longitude", "date","cha_brandID"};

            String[] args = new String[]{charityID+""};

            this.db= this.conexion.getWritableDatabase();

            cursor=this.db.query("charity", campos, "charityID=?", args, null, null, null);

            if(cursor.moveToFirst()){
                do{
                    charity = new Charity(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4),cursor.getFloat(5),cursor.getFloat(6), format.parse(cursor.getString(7)), cursor.getInt(8));

                }while(cursor.moveToNext());

            }

            db.close();
            conexion.close();

            return charity;
        }catch(Exception e){
            e.printStackTrace();
            return charity;
        }finally{
            if(cursor!=null) cursor.close();
            this.db.close();
            this.conexion.close();
        }

    }

    public ArrayList<Charity> selectCharities(){

		ArrayList <Charity> charities = new ArrayList <Charity>();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Cursor cursor=null;

        try{
			String [] campos=new String [] {"charityID","title","description","address","postcode","latitude","longitude", "date","cha_brandID"};

			this.db= this.conexion.getWritableDatabase();

			cursor=this.db.query("charity", campos, null, null, null, null, null);

            if(cursor.moveToFirst()){
                do{
                    Charity charity = new Charity(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4),cursor.getFloat(5),cursor.getFloat(6), format.parse(cursor.getString(7)), cursor.getInt(8));
                    charities.add(cursor.getInt(0),charity);

                }while(cursor.moveToNext());

            }

            db.close();
            conexion.close();

            return charities;
        }catch(Exception e){
            e.printStackTrace();
            return charities;
        }finally{
            cursor.close();
            this.db.close();
            this.conexion.close();
        }
    }

    public ArrayList<Charity> selectCharities(double nlat, double slat, double wlong, double elong){

        ArrayList <Charity> charities = new ArrayList <Charity>();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Cursor cursor=null;

        try{
            String [] campos=new String [] {"charityID","title","description","address","postcode","latitude","longitude", "date","cha_brandID"};

            String[] args=new String[] {nlat+"",slat+"",wlong+"",elong+""};

            this.db= this.conexion.getWritableDatabase();

            cursor=this.db.query("charity", campos, "latitude <= ? and latitude >= ? and longitude >= ? and longitude <= ?", args, null, null, null);

            if(cursor.moveToFirst()){
                do{
                    Charity charity = new Charity(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4),cursor.getFloat(5),cursor.getFloat(6), format.parse(cursor.getString(7)), cursor.getInt(8));
                    charities.add(charity);

                }while(cursor.moveToNext());

            }

            db.close();
            conexion.close();

            return charities;
        }catch(Exception e){
            e.printStackTrace();
            return charities;
        }finally{
            if(cursor != null){
                cursor.close();
            }
            if(this.db != null){
                this.db.close();
            }
            if(this.conexion != null){
                this.conexion.close();
            }

        }
    }

    public boolean updateCharity(Charity charity){

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try{

            this.db= this.conexion.getWritableDatabase();

            String [] args=new String[] {String.valueOf(charity.getCharityID())};

            ContentValues cv=new ContentValues();
            cv.put("title", charity.getTitle());
            cv.put("description", charity.getDescription());
            cv.put("address", charity.getAddress());
            cv.put("postcode", charity.getPostcode());
            cv.put("latitude", charity.getLatitude());
            cv.put("longitude", charity.getLongitude());
            cv.put("date", format.format(charity.getDate()));
            cv.put("cha_brandID", charity.getChaBrandID());

            int status=this.db.update("charity", cv, "charityID=?", args);

            this.db.close();
            this.conexion.close();
            return (status!=0);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }


    public boolean bulk(ArrayList<Charity> charities){

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try{
            this.db=conexion.getWritableDatabase();

            String sql = "INSERT INTO charity VALUES (?,?,?,?,?,?,?,?,?);";
            SQLiteStatement statement = this.db.compileStatement(sql);
            this.db.beginTransaction();
            for(Charity charity : charities){
                statement.clearBindings();
                statement.bindLong(1, charity.getCharityID());
                statement.bindString(2, charity.getTitle());
                statement.bindString(3, charity.getDescription());
                statement.bindString(4, charity.getAddress());
                statement.bindString(5, charity.getPostcode());
                statement.bindDouble(6, charity.getLatitude());
                statement.bindDouble(7, charity.getLongitude());
                statement.bindString(8, format.format(charity.getDate()));
                statement.bindLong(9, charity.getChaBrandID());
                statement.execute();
            }
            db.setTransactionSuccessful();
            db.endTransaction();

            db.close();
            this.conexion.close();

            return true;

        }catch(Exception e){

            db.endTransaction();
            this.db.close();
            this.conexion.close();
            return false;
        }


    }

}
