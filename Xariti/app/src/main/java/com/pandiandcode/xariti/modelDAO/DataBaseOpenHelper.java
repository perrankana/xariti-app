package com.pandiandcode.xariti.modelDAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Perrankana on 05/01/15.
 */
public class DataBaseOpenHelper extends SQLiteOpenHelper {

    final private static String TABLE_CHARITY =

            "CREATE TABLE charity ("
                    + "charityID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "title TEXT NOT NULL, "
                    + "description TEXT NOT NULL, "
                    + "address TEXT NOT NULL, "
                    + "postcode TEXT NOT NULL, "
                    + "latitude NUMERIC NOT NULL,"
                    + "longitude NUMERIC NOT NULL,"
                    + "date TEXT NOT NULL, "
                    + "cha_brandID INTEGER )";

    final private static String TABLE_LINK =

            "CREATE TABLE link ("
                    + "linkID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "title TEXT NOT NULL, "
                    + "link TEXT NOT NULL, "
                    + "charityID INTEGER )";

    final private static String TABLE_CHA_BRAND =

            "CREATE TABLE chaBrand ("
                    + "cha_brandID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "title TEXT NOT NULL, "
                    + "image TEXT NOT NULL, "
                    + "description TEXT NOT NULL, "
                    + "website TEXT NOT NULL )";


    final private static String TABLE_ARTICLE =

            "CREATE TABLE article ("
                    + "articleID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "title TEXT NOT NULL, "
                    + "description TEXT NOT NULL, "
                    + "image TEXT NOT NULL, "
                    + "date TEXT NOT NULL, "
                    + "charityID INTEGER, "
                    + "imageW INTEGER, "
                    + "imageH INTEGER  )";

    final private static String NAME = "Xariti_db";
    final private static Integer VERSION = 1;
    final private Context mContext;

    private static DataBaseOpenHelper instance;

    public static synchronized DataBaseOpenHelper getHelper(Context context)
    {
        if (instance == null)
            instance = new DataBaseOpenHelper(context);

        return instance;
    }

    public DataBaseOpenHelper(Context context) {
        super(context, NAME, null, VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CHARITY);
        db.execSQL(TABLE_LINK);
        db.execSQL(TABLE_CHA_BRAND);
        db.execSQL(TABLE_ARTICLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
