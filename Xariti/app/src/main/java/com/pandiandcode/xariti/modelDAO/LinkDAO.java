package com.pandiandcode.xariti.modelDAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.pandiandcode.xariti.model.Link;

import java.util.ArrayList;

/**
 * Created by Perrankana on 06/01/15.
 */
public class LinkDAO {

    private DataBaseOpenHelper conexion;
    private SQLiteDatabase db;
    private Context context;

    public LinkDAO(Context context){
        this.context = context;
        try{
            this.conexion= DataBaseOpenHelper.getHelper(context);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean insert(Link link){
        try{
            this.db=conexion.getWritableDatabase();

            ContentValues nr=new ContentValues();
            nr.put("linkID", link.getLinkID());
            nr.put("title", link.getTitle());
            nr.put("link", link.getLink());
            nr.put("charityID", link.getCharityID());

            int status=(int) this.db.insert("link", null, nr);
            db.close();
            conexion.close();

            return (status!=-1);

        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean delete(int linkID){

        try{
            this.db= this.conexion.getWritableDatabase();

            String [] args=new String[] {linkID+""};

            int status=this.db.delete("link","linkID=?",args);
            db.close();
            conexion.close();

            return (status!=0);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean deleteAll(int charityID){

        try{
            this.db= this.conexion.getWritableDatabase();

            String [] args=new String[] {charityID+""};

            int status=this.db.delete("link","charityID=?",args);
            db.close();
            conexion.close();

            return (status!=0);

        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public boolean updateLink(Link link){
        try{

            this.db= this.conexion.getWritableDatabase();

            String [] args=new String[] {String.valueOf(link.getLinkID())};

            ContentValues cv=new ContentValues();
            cv.put("title", link.getTitle());
            cv.put("link", link.getLink());

            int status=this.db.update("link", cv, "linkID=?", args);

            db.close();
            conexion.close();


            return (status!=0);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.db.close();
            this.conexion.close();
        }
    }

    public ArrayList<Link> selectLinks(int charityID){

        Cursor cursor=null;
        ArrayList <Link> links = new ArrayList <Link>();

        try{
            String [] campos=new String [] {"linkID","title","link", "charityID"};

            String[] args=new String[] {charityID+""};

            this.db= this.conexion.getWritableDatabase();

            cursor=this.db.query("link", campos, "charityID=?", args, null, null, null);

            if(cursor.moveToFirst()){
                do{

                    links.add(new Link(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3)));

                }while(cursor.moveToNext());
            }

            db.close();
            conexion.close();

            return links;
        }catch(Exception e){
            e.printStackTrace();
            return links;
        }finally{
            if(cursor!=null){
                cursor.close();
            }

            this.db.close();
            this.conexion.close();
        }
    }

    public boolean bulk(ArrayList<Link> links){

        try{
            this.db=conexion.getWritableDatabase();

            String sql = "INSERT INTO link VALUES (?,?,?,?);";
            SQLiteStatement statement = this.db.compileStatement(sql);
            this.db.beginTransaction();
            for(Link link : links){
                statement.clearBindings();
                statement.bindLong(1,link.getLinkID());
                statement.bindString(2,link.getTitle());
                statement.bindString(3,link.getLink());
                statement.bindLong(4,link.getCharityID());
                statement.execute();
            }
            db.setTransactionSuccessful();
            db.endTransaction();

            db.close();
            conexion.close();

            return true;

        }catch(Exception e){
            db.endTransaction();
            if(db!=null){this.db.close();}
            if(this.conexion!=null){this.conexion.close();}
            return false;
        }

    }


}
