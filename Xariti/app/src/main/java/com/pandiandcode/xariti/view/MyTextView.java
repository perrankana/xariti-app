package com.pandiandcode.xariti.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Perrankana on 12/03/15.
 */
public class MyTextView extends TextView {

    public MyTextView( Context context ) {

        super( context );
        Typeface mFace = Typeface.createFromAsset( context.getAssets(), "fonts/comfortaa_regular-webfont.ttf" );
        this.setTypeface( mFace );

    }

    public MyTextView( Context context, AttributeSet attrs ) {

        super( context, attrs );
        Typeface mFace = Typeface.createFromAsset( context.getAssets(), "fonts/comfortaa_regular-webfont.ttf" );
        this.setTypeface( mFace );

    }

    public MyTextView( Context context, AttributeSet attrs, int defStyle ) {

        super( context, attrs, defStyle );
        Typeface mFace = Typeface.createFromAsset( context.getAssets(), "fonts/comfortaa_regular-webfont.ttf" );
        this.setTypeface( mFace );

    }

    protected void onDraw ( Canvas canvas ) { super.onDraw(canvas); }
}
